﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SPMTool.Models
{
    public class Requirement : Base
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; }
        public DateTime? ExpectedEndDate { get; set; }
        public DateTime? ExpectedStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public bool IsCompleted { get; set; }
        public int? AssignedToUserId { get; set; }
        [ForeignKey("AssignedToUserId")]
        public virtual User AssignedToUser { get; set; }
        public int? RequirementTypeId { get; set; }
        [ForeignKey("RequirementTypeId")]
        public virtual RequirementType RequirementType { get; set; }
        public int ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }


        [InverseProperty("Requirement")]
        public virtual List<RequirementDependency> Requirements { get; set; }
        [InverseProperty("DependencyRequirement")]
        public virtual List<RequirementDependency> RequirementDependencies { get; set; }

        public override string ToString()
        {
            return Code + "-" + Description;
        }
    }
}
