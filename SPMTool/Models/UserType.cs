﻿using System.Collections.Generic;

namespace SPMTool.Models
{
    public class UserType : Base
    {
        public string UserTypeName { get; set; }

        public virtual IEnumerable<User> Users { get; set; }

    }
}
