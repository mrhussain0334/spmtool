﻿using System.Collections.Generic;

namespace SPMTool.Models
{
    public class ParentRequriement : Base
    {
        
        public string Header { get; set; }

        public virtual IEnumerable<Requirement> Requirements { get; set; }
    }
}
