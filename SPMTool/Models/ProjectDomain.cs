﻿using System.Collections.Generic;

namespace SPMTool.Models
{
    public class ProjectDomain : Base
    {
        public string ProjectDomainName { get; set; }
        public virtual IEnumerable<Project> Projects { get; set; }

        public override string ToString()
        {
            return ProjectDomainName;
        }
    }
}
