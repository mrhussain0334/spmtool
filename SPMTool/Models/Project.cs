﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SPMTool.Models
{
    public class Project : Base
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProjectDomainId { get; set; }
        [ForeignKey("ProjectDomainId")]
        public virtual ProjectDomain ProjectDomain { get; set; }
        public int ProjectTypeId { get; set; }
        [ForeignKey("ProjectTypeId")]
        public virtual ProjectType ProjectType { get; set; }
        public int? ProjectManagerId { get; set; }
        [ForeignKey("ProjectManagerId")]
        public virtual ProjectManager ProjectManager { get; set; }
        public virtual IList<ProjectCharter> ProjectCharters { get; set; }
        public virtual IEnumerable<Requirement> Requirements { get; set; }
        public virtual IEnumerable<Wbs> Wbses { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
