﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPMTool.Models
{
    public class Base
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }
    }
}
