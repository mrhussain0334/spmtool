﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SPMTool.Models
{
    public class Wbs : Base
    {
        public int ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Package { get; set; }
    }
}
