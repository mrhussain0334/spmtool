﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SPMTool.Models
{
    public class ProjectCharter : Base
    {
        public string ProjectName { get; set; }
        public string Requester { get; set; }
        public string Author { get; set; }
        public string Sponser { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public string Purpose { get; set; }
        public string Goals { get; set; }
        public string Scope { get; set; }
        public string StakeHolders { get; set; }
        public string Risks { get; set; }
        public string Assumptions { get; set; }
        public string BudgetRequirements { get; set; }
        public int ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

    }
}
