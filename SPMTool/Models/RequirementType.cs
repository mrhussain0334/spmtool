﻿using System.Collections.Generic;

namespace SPMTool.Models
{
    public class RequirementType : Base
    {
        public string RequirementTypeName { get; set; }
        public virtual IEnumerable<Requirement> Requirements { get; set; }

        public override string ToString()
        {
            return RequirementTypeName;
        }
    }
}
