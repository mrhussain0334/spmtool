﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SPMTool.Models
{
    public class User : Base
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Designation { get; set; }
        public int CreatedByProjectManagerId { get; set; }
        [ForeignKey("CreatedByProjectManagerId")]
        public virtual ProjectManager CreatedByProjectManager { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
