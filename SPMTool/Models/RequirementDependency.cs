﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPMTool.Models
{
    public class RequirementDependency : Base
    {
        public int? RequirementId { get; set; }
        [ForeignKey("RequirementId")]
        public virtual Requirement Requirement { get; set; }

        public int? DependencyRequirementId { get; set; }
        [ForeignKey("DependencyRequirementId")]
        public virtual Requirement DependencyRequirement { get; set; }

    }
}
