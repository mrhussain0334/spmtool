﻿using System.Collections.Generic;

namespace SPMTool.Models
{
    public class ProjectType : Base
    {
        public string ProjectTypeName { get; set; }
        public virtual IEnumerable<Project> Projects { get; set; }

        public override string ToString()
        {
            return ProjectTypeName;
        }
    }
}
