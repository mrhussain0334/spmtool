namespace SPMTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requirements", "DependentId", c => c.Int());
            CreateIndex("dbo.Requirements", "DependentId");
            AddForeignKey("dbo.Requirements", "DependentId", "dbo.Requirements", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requirements", "DependentId", "dbo.Requirements");
            DropIndex("dbo.Requirements", new[] { "DependentId" });
            DropColumn("dbo.Requirements", "DependentId");
        }
    }
}
