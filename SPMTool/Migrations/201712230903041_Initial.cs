namespace SPMTool.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ParentRequriements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Header = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectCharters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(),
                        Requester = c.String(),
                        Author = c.String(),
                        Sponser = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Description = c.String(),
                        Purpose = c.String(),
                        Goals = c.String(),
                        Scope = c.String(),
                        StakeHolders = c.String(),
                        Risks = c.String(),
                        Assumptions = c.String(),
                        BudgetRequirements = c.String(),
                        ProjectId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        ProjectDomainId = c.Int(nullable: false),
                        ProjectTypeId = c.Int(nullable: false),
                        ProjectManagerId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProjectDomains", t => t.ProjectDomainId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectManagers", t => t.ProjectManagerId)
                .ForeignKey("dbo.ProjectTypes", t => t.ProjectTypeId, cascadeDelete: true)
                .Index(t => t.ProjectDomainId)
                .Index(t => t.ProjectTypeId)
                .Index(t => t.ProjectManagerId);
            
            CreateTable(
                "dbo.ProjectDomains",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectDomainName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectManagers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.String(),
                        ContactNumber = c.String(),
                        Address = c.String(),
                        Qualifications = c.String(),
                        CreatedByAdminId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Admins", t => t.CreatedByAdminId, cascadeDelete: true)
                .Index(t => t.CreatedByAdminId);
            
            CreateTable(
                "dbo.ProjectTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectTypeName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectClients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CompanyName = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Requirements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                        Deadline = c.String(),
                        ExpectedEndDate = c.DateTime(),
                        ExpectedStartDate = c.DateTime(),
                        ActualEndDate = c.DateTime(),
                        ActualStartDate = c.DateTime(),
                        IsCompleted = c.Boolean(nullable: false),
                        AssignedToUserId = c.Int(),
                        RequirementTypeId = c.Int(),
                        ProjectId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AssignedToUserId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.RequirementTypes", t => t.RequirementTypeId)
                .Index(t => t.AssignedToUserId)
                .Index(t => t.RequirementTypeId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.String(),
                        Address = c.String(),
                        Designation = c.String(),
                        CreatedByProjectManagerId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProjectManagers", t => t.CreatedByProjectManagerId, cascadeDelete: true)
                .Index(t => t.CreatedByProjectManagerId);
            
            CreateTable(
                "dbo.RequirementTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequirementTypeName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRequirements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserTypeName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Wbs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        Code = c.String(),
                        Description = c.String(),
                        Package = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Wbs", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Requirements", "RequirementTypeId", "dbo.RequirementTypes");
            DropForeignKey("dbo.Requirements", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Requirements", "AssignedToUserId", "dbo.Users");
            DropForeignKey("dbo.Users", "CreatedByProjectManagerId", "dbo.ProjectManagers");
            DropForeignKey("dbo.ProjectCharters", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "ProjectTypeId", "dbo.ProjectTypes");
            DropForeignKey("dbo.Projects", "ProjectManagerId", "dbo.ProjectManagers");
            DropForeignKey("dbo.ProjectManagers", "CreatedByAdminId", "dbo.Admins");
            DropForeignKey("dbo.Projects", "ProjectDomainId", "dbo.ProjectDomains");
            DropIndex("dbo.Wbs", new[] { "ProjectId" });
            DropIndex("dbo.Users", new[] { "CreatedByProjectManagerId" });
            DropIndex("dbo.Requirements", new[] { "ProjectId" });
            DropIndex("dbo.Requirements", new[] { "RequirementTypeId" });
            DropIndex("dbo.Requirements", new[] { "AssignedToUserId" });
            DropIndex("dbo.ProjectManagers", new[] { "CreatedByAdminId" });
            DropIndex("dbo.Projects", new[] { "ProjectManagerId" });
            DropIndex("dbo.Projects", new[] { "ProjectTypeId" });
            DropIndex("dbo.Projects", new[] { "ProjectDomainId" });
            DropIndex("dbo.ProjectCharters", new[] { "ProjectId" });
            DropTable("dbo.Wbs");
            DropTable("dbo.UserTypes");
            DropTable("dbo.UserRequirements");
            DropTable("dbo.RequirementTypes");
            DropTable("dbo.Users");
            DropTable("dbo.Requirements");
            DropTable("dbo.ProjectClients");
            DropTable("dbo.ProjectTypes");
            DropTable("dbo.ProjectManagers");
            DropTable("dbo.ProjectDomains");
            DropTable("dbo.Projects");
            DropTable("dbo.ProjectCharters");
            DropTable("dbo.ParentRequriements");
            DropTable("dbo.Admins");
        }
    }
}
