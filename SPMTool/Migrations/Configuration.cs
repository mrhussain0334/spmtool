using SPMTool.Context;
using SPMTool.Models;
using System;
using System.Data.Entity.Migrations;
using SPMTool.Services;

namespace SPMTool.Migrations
{
    
    internal sealed class Configuration : DbMigrationsConfiguration<MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MyDbContext context)
        {
            var db = new MyDbContext();
            var domainTypes = new[]
            {
                "System software",
                "Engineering/scientific software",
                "Embedded software",
                "Application software",
                "Web Applications",
                "Artificial Intelligence Software",
                "Mobile Applications",
                "Product-line software",
            };
            var projecttype = new ProjectType()
            {
                ProjectTypeName = "Research and Development",
                CreatedAt = DateTime.Now,
                UpdateAt = DateTime.Now
            };
            db.ProjectTypes.Add(projecttype);
            projecttype = new ProjectType()
            {
                ProjectTypeName = "Development",
                CreatedAt = DateTime.Now,
                UpdateAt = DateTime.Now
            };
            db.ProjectTypes.Add(projecttype);


            foreach (var domainType in domainTypes)
            {
                var domain = new ProjectDomain()
                {
                    ProjectDomainName = domainType,
                    UpdateAt = DateTime.Now,
                    CreatedAt = DateTime.Now
                };
                db.ProjectDomains.Add(domain);
            }

            var admin = new Admin()
            {
                UpdateAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Email = "h.halai0334@gmail.com",
                FirstName = "Hussain",
                Gender = 'M',
                LastName = "Murtaza",
                Password = PasswordService.EncryptPassword("123"),
                Username = "Admin"
            };
            db.Admins.Add(admin);
            db.RequirementTypes.Add(new RequirementType()
            {
                CreatedAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                RequirementTypeName = "Main"
            });
            db.RequirementTypes.Add(new RequirementType()
            {
                CreatedAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                RequirementTypeName = "Sub"
            });
            db.SaveChanges();
        }
    }
}
