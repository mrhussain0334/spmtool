namespace SPMTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUpdate1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requirements", "DependentId", "dbo.Requirements");
            DropIndex("dbo.Requirements", new[] { "DependentId" });
            DropColumn("dbo.Requirements", "DependentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Requirements", "DependentId", c => c.Int());
            CreateIndex("dbo.Requirements", "DependentId");
            AddForeignKey("dbo.Requirements", "DependentId", "dbo.Requirements", "Id");
        }
    }
}
