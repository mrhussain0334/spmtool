namespace SPMTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDependency1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RequirementDependencies", "DependencyRequirementId", "dbo.Requirements");
            DropForeignKey("dbo.RequirementDependencies", "RequirementId", "dbo.Requirements");
            DropIndex("dbo.RequirementDependencies", new[] { "RequirementId" });
            DropIndex("dbo.RequirementDependencies", new[] { "DependencyRequirementId" });
            AlterColumn("dbo.RequirementDependencies", "RequirementId", c => c.Int());
            AlterColumn("dbo.RequirementDependencies", "DependencyRequirementId", c => c.Int());
            CreateIndex("dbo.RequirementDependencies", "RequirementId");
            CreateIndex("dbo.RequirementDependencies", "DependencyRequirementId");
            AddForeignKey("dbo.RequirementDependencies", "DependencyRequirementId", "dbo.Requirements", "Id");
            AddForeignKey("dbo.RequirementDependencies", "RequirementId", "dbo.Requirements", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequirementDependencies", "RequirementId", "dbo.Requirements");
            DropForeignKey("dbo.RequirementDependencies", "DependencyRequirementId", "dbo.Requirements");
            DropIndex("dbo.RequirementDependencies", new[] { "DependencyRequirementId" });
            DropIndex("dbo.RequirementDependencies", new[] { "RequirementId" });
            AlterColumn("dbo.RequirementDependencies", "DependencyRequirementId", c => c.Int(nullable: false));
            AlterColumn("dbo.RequirementDependencies", "RequirementId", c => c.Int(nullable: false));
            CreateIndex("dbo.RequirementDependencies", "DependencyRequirementId");
            CreateIndex("dbo.RequirementDependencies", "RequirementId");
            AddForeignKey("dbo.RequirementDependencies", "RequirementId", "dbo.Requirements", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RequirementDependencies", "DependencyRequirementId", "dbo.Requirements", "Id", cascadeDelete: true);
        }
    }
}
