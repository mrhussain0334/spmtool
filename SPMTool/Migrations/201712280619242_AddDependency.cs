namespace SPMTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDependency : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RequirementDependencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequirementId = c.Int(nullable: false),
                        DependencyRequirementId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdateAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requirements", t => t.DependencyRequirementId)
                .ForeignKey("dbo.Requirements", t => t.RequirementId)
                .Index(t => t.RequirementId)
                .Index(t => t.DependencyRequirementId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequirementDependencies", "RequirementId", "dbo.Requirements");
            DropForeignKey("dbo.RequirementDependencies", "DependencyRequirementId", "dbo.Requirements");
            DropIndex("dbo.RequirementDependencies", new[] { "DependencyRequirementId" });
            DropIndex("dbo.RequirementDependencies", new[] { "RequirementId" });
            DropTable("dbo.RequirementDependencies");
        }
    }
}
