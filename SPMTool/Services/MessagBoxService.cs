﻿using System;
using System.Windows.Forms;
using MetroFramework;

namespace SPMTool.Services
{
    public class MessagBoxService
    {
        public static void Show(IWin32Window owner, Exception message)
        {
            MetroMessageBox.Show(owner, message.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
        public static void Show(IWin32Window owner,string message,string title)
        {
            MetroMessageBox.Show(owner, message, title, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public static DialogResult Confirm(IWin32Window owner, string message, string title)
        {
            return MetroMessageBox.Show(owner, message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
        }
    }
}
