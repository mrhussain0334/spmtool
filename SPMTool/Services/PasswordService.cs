﻿using System.Security.Cryptography;
using System.Text;

namespace SPMTool.Services
{
    public class PasswordService
    {
        public static string EncryptPassword(string password)
        {
            var md5 = MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (var t in hash)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
