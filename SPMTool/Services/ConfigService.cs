﻿using SPMTool.Models;

namespace SPMTool.Services
{
    public class ConfigService
    {

        private static ConfigService _instance = null;

        public static ConfigService Instance => _instance ?? (_instance = new ConfigService());

        public string Name { get; set; }
        public int Id { get; set; }
        public void SetValues(User user)
        {
            Name = user.FirstName;
            Id = user.Id;
        }
        public void SetValues(Admin user)
        {
            Name = user.FirstName;
            Id = user.Id;
        }
        public void SetValues(ProjectManager user)
        {
            Name = user.FirstName;
            Id = user.Id;
        }
    }
}
