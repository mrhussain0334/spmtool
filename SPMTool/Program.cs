﻿using System;
using System.Windows.Forms;
using SPMTool.Forms;
using SPMTool.Forms.Charts;

namespace SPMTool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }
    }
}
