﻿namespace SPMTool.UserControls
{
    partial class AllWBS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridWbs = new MetroFramework.Controls.MetroGrid();
            this.projectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updateAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wbsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnImportWBS = new MetroFramework.Controls.MetroButton();
            this.txtCode = new MetroFramework.Controls.MetroTextBox();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.btnAddWBS = new MetroFramework.Controls.MetroButton();
            this.btnDeleteWBS = new MetroFramework.Controls.MetroButton();
            this.txtPackage = new MetroFramework.Controls.MetroTextBox();
            this.btnGenerate = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridWbs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wbsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridWbs
            // 
            this.gridWbs.AllowUserToResizeRows = false;
            this.gridWbs.AutoGenerateColumns = false;
            this.gridWbs.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridWbs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridWbs.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridWbs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridWbs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridWbs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridWbs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.projectDataGridViewTextBoxColumn,
            this.codeDataGridViewTextBoxColumn,
            this.packageDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.createdAtDataGridViewTextBoxColumn,
            this.updateAtDataGridViewTextBoxColumn});
            this.gridWbs.DataSource = this.wbsBindingSource;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridWbs.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridWbs.EnableHeadersVisualStyles = false;
            this.gridWbs.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridWbs.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridWbs.Location = new System.Drawing.Point(3, 161);
            this.gridWbs.Name = "gridWbs";
            this.gridWbs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridWbs.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridWbs.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridWbs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridWbs.Size = new System.Drawing.Size(597, 352);
            this.gridWbs.TabIndex = 0;
            // 
            // projectDataGridViewTextBoxColumn
            // 
            this.projectDataGridViewTextBoxColumn.DataPropertyName = "Project";
            this.projectDataGridViewTextBoxColumn.HeaderText = "Project";
            this.projectDataGridViewTextBoxColumn.Name = "projectDataGridViewTextBoxColumn";
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "Code";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            // 
            // packageDataGridViewTextBoxColumn
            // 
            this.packageDataGridViewTextBoxColumn.DataPropertyName = "Package";
            this.packageDataGridViewTextBoxColumn.HeaderText = "Package";
            this.packageDataGridViewTextBoxColumn.Name = "packageDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // createdAtDataGridViewTextBoxColumn
            // 
            this.createdAtDataGridViewTextBoxColumn.DataPropertyName = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.HeaderText = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.Name = "createdAtDataGridViewTextBoxColumn";
            // 
            // updateAtDataGridViewTextBoxColumn
            // 
            this.updateAtDataGridViewTextBoxColumn.DataPropertyName = "UpdateAt";
            this.updateAtDataGridViewTextBoxColumn.HeaderText = "UpdateAt";
            this.updateAtDataGridViewTextBoxColumn.Name = "updateAtDataGridViewTextBoxColumn";
            // 
            // wbsBindingSource
            // 
            this.wbsBindingSource.DataSource = typeof(SPMTool.Models.Wbs);
            // 
            // btnImportWBS
            // 
            this.btnImportWBS.Location = new System.Drawing.Point(3, 14);
            this.btnImportWBS.Name = "btnImportWBS";
            this.btnImportWBS.Size = new System.Drawing.Size(136, 33);
            this.btnImportWBS.TabIndex = 12;
            this.btnImportWBS.Text = "Import WBS";
            this.btnImportWBS.UseSelectable = true;
            this.btnImportWBS.Click += new System.EventHandler(this.btnImportWBS_Click);
            // 
            // txtCode
            // 
            // 
            // 
            // 
            this.txtCode.CustomButton.Image = null;
            this.txtCode.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.txtCode.CustomButton.Name = "";
            this.txtCode.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCode.CustomButton.TabIndex = 1;
            this.txtCode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCode.CustomButton.UseSelectable = true;
            this.txtCode.CustomButton.Visible = false;
            this.txtCode.Lines = new string[0];
            this.txtCode.Location = new System.Drawing.Point(3, 99);
            this.txtCode.MaxLength = 32767;
            this.txtCode.Name = "txtCode";
            this.txtCode.PasswordChar = '\0';
            this.txtCode.PromptText = "Enter Code";
            this.txtCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCode.SelectedText = "";
            this.txtCode.SelectionLength = 0;
            this.txtCode.SelectionStart = 0;
            this.txtCode.ShortcutsEnabled = true;
            this.txtCode.Size = new System.Drawing.Size(75, 23);
            this.txtCode.TabIndex = 13;
            this.txtCode.UseSelectable = true;
            this.txtCode.WaterMark = "Enter Code";
            this.txtCode.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCode.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.CustomButton.Image = null;
            this.txtDescription.CustomButton.Location = new System.Drawing.Point(341, 1);
            this.txtDescription.CustomButton.Name = "";
            this.txtDescription.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDescription.CustomButton.TabIndex = 1;
            this.txtDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDescription.CustomButton.UseSelectable = true;
            this.txtDescription.CustomButton.Visible = false;
            this.txtDescription.Lines = new string[0];
            this.txtDescription.Location = new System.Drawing.Point(237, 99);
            this.txtDescription.MaxLength = 32767;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.PasswordChar = '\0';
            this.txtDescription.PromptText = "Enter Description";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescription.SelectedText = "";
            this.txtDescription.SelectionLength = 0;
            this.txtDescription.SelectionStart = 0;
            this.txtDescription.ShortcutsEnabled = true;
            this.txtDescription.Size = new System.Drawing.Size(363, 23);
            this.txtDescription.TabIndex = 14;
            this.txtDescription.UseSelectable = true;
            this.txtDescription.WaterMark = "Enter Description";
            this.txtDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnAddWBS
            // 
            this.btnAddWBS.Location = new System.Drawing.Point(0, 128);
            this.btnAddWBS.Name = "btnAddWBS";
            this.btnAddWBS.Size = new System.Drawing.Size(136, 27);
            this.btnAddWBS.TabIndex = 15;
            this.btnAddWBS.Text = "Add WBS";
            this.btnAddWBS.UseSelectable = true;
            this.btnAddWBS.Click += new System.EventHandler(this.btnAddWBS_Click);
            // 
            // btnDeleteWBS
            // 
            this.btnDeleteWBS.Location = new System.Drawing.Point(142, 128);
            this.btnDeleteWBS.Name = "btnDeleteWBS";
            this.btnDeleteWBS.Size = new System.Drawing.Size(136, 27);
            this.btnDeleteWBS.TabIndex = 16;
            this.btnDeleteWBS.Text = "Delete WBS";
            this.btnDeleteWBS.UseSelectable = true;
            this.btnDeleteWBS.Click += new System.EventHandler(this.btnDeleteWBS_Click);
            // 
            // txtPackage
            // 
            // 
            // 
            // 
            this.txtPackage.CustomButton.Image = null;
            this.txtPackage.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtPackage.CustomButton.Name = "";
            this.txtPackage.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPackage.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPackage.CustomButton.TabIndex = 1;
            this.txtPackage.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPackage.CustomButton.UseSelectable = true;
            this.txtPackage.CustomButton.Visible = false;
            this.txtPackage.Lines = new string[0];
            this.txtPackage.Location = new System.Drawing.Point(84, 99);
            this.txtPackage.MaxLength = 32767;
            this.txtPackage.Name = "txtPackage";
            this.txtPackage.PasswordChar = '\0';
            this.txtPackage.PromptText = "Enter Package";
            this.txtPackage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPackage.SelectedText = "";
            this.txtPackage.SelectionLength = 0;
            this.txtPackage.SelectionStart = 0;
            this.txtPackage.ShortcutsEnabled = true;
            this.txtPackage.Size = new System.Drawing.Size(147, 23);
            this.txtPackage.TabIndex = 17;
            this.txtPackage.UseSelectable = true;
            this.txtPackage.WaterMark = "Enter Package";
            this.txtPackage.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPackage.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(145, 14);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(136, 33);
            this.btnGenerate.TabIndex = 18;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseSelectable = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // AllWBS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.txtPackage);
            this.Controls.Add(this.btnDeleteWBS);
            this.Controls.Add(this.btnAddWBS);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.btnImportWBS);
            this.Controls.Add(this.gridWbs);
            this.Name = "AllWBS";
            this.Size = new System.Drawing.Size(603, 516);
            this.Load += new System.EventHandler(this.AllWBS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridWbs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wbsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroGrid gridWbs;
        private MetroFramework.Controls.MetroButton btnImportWBS;
        private MetroFramework.Controls.MetroTextBox txtCode;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private MetroFramework.Controls.MetroButton btnAddWBS;
        private MetroFramework.Controls.MetroButton btnDeleteWBS;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn updateAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource wbsBindingSource;
        private MetroFramework.Controls.MetroTextBox txtPackage;
        private MetroFramework.Controls.MetroButton btnGenerate;
    }
}
