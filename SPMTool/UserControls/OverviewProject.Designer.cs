﻿namespace SPMTool.UserControls
{
    partial class OverviewProject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.lblManager = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.lblAuthor = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.lblDomain = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.lblType = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.lblRequester = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.lblSponser = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblEndDate = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.lblStartDate = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.lblProjectName = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtDetails = new MetroFramework.Controls.MetroTextBox();
            this.txtPurpose = new MetroFramework.Controls.MetroTextBox();
            this.txtScope = new MetroFramework.Controls.MetroTextBox();
            this.txtStackholder = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroPanel1.Controls.Add(this.txtStackholder);
            this.metroPanel1.Controls.Add(this.txtScope);
            this.metroPanel1.Controls.Add(this.txtPurpose);
            this.metroPanel1.Controls.Add(this.txtDetails);
            this.metroPanel1.Controls.Add(this.metroLabel14);
            this.metroPanel1.Controls.Add(this.metroLabel13);
            this.metroPanel1.Controls.Add(this.metroLabel12);
            this.metroPanel1.Controls.Add(this.metroLabel10);
            this.metroPanel1.Controls.Add(this.lblManager);
            this.metroPanel1.Controls.Add(this.metroLabel11);
            this.metroPanel1.Controls.Add(this.lblAuthor);
            this.metroPanel1.Controls.Add(this.metroLabel8);
            this.metroPanel1.Controls.Add(this.lblDomain);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.lblType);
            this.metroPanel1.Controls.Add(this.metroLabel9);
            this.metroPanel1.Controls.Add(this.lblRequester);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Controls.Add(this.lblSponser);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.lblEndDate);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.lblStartDate);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.lblProjectName);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(770, 525);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            this.metroPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.metroPanel1_Paint);
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(20, 456);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(82, 19);
            this.metroLabel14.TabIndex = 26;
            this.metroLabel14.Text = "Stackholders";
            this.metroLabel14.WrapToLine = true;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(20, 364);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(45, 19);
            this.metroLabel13.TabIndex = 24;
            this.metroLabel13.Text = "Scope";
            this.metroLabel13.WrapToLine = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(20, 264);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(57, 19);
            this.metroLabel12.TabIndex = 22;
            this.metroLabel12.Text = "Purpose";
            this.metroLabel12.WrapToLine = true;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(20, 186);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(47, 19);
            this.metroLabel10.TabIndex = 20;
            this.metroLabel10.Text = "Details";
            this.metroLabel10.WrapToLine = true;
            // 
            // lblManager
            // 
            this.lblManager.AutoSize = true;
            this.lblManager.Location = new System.Drawing.Point(452, 50);
            this.lblManager.Name = "lblManager";
            this.lblManager.Size = new System.Drawing.Size(90, 19);
            this.lblManager.TabIndex = 19;
            this.lblManager.Text = "Project Name";
            this.lblManager.WrapToLine = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(309, 50);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(107, 19);
            this.metroLabel11.TabIndex = 18;
            this.metroLabel11.Text = "Project Manager";
            this.metroLabel11.WrapToLine = true;
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.Location = new System.Drawing.Point(452, 84);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(90, 19);
            this.lblAuthor.TabIndex = 17;
            this.lblAuthor.Text = "Project Name";
            this.lblAuthor.WrapToLine = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(309, 84);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(94, 19);
            this.metroLabel8.TabIndex = 16;
            this.metroLabel8.Text = "Project Author";
            this.metroLabel8.WrapToLine = true;
            // 
            // lblDomain
            // 
            this.lblDomain.AutoSize = true;
            this.lblDomain.Location = new System.Drawing.Point(452, 152);
            this.lblDomain.Name = "lblDomain";
            this.lblDomain.Size = new System.Drawing.Size(90, 19);
            this.lblDomain.TabIndex = 15;
            this.lblDomain.Text = "Project Name";
            this.lblDomain.WrapToLine = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(309, 152);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(100, 19);
            this.metroLabel7.TabIndex = 14;
            this.metroLabel7.Text = "Project Domain";
            this.metroLabel7.WrapToLine = true;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(452, 118);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(90, 19);
            this.lblType.TabIndex = 13;
            this.lblType.Text = "Project Name";
            this.lblType.WrapToLine = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(309, 118);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(81, 19);
            this.metroLabel9.TabIndex = 12;
            this.metroLabel9.Text = "Project Type";
            this.metroLabel9.WrapToLine = true;
            // 
            // lblRequester
            // 
            this.lblRequester.AutoSize = true;
            this.lblRequester.Location = new System.Drawing.Point(125, 153);
            this.lblRequester.Name = "lblRequester";
            this.lblRequester.Size = new System.Drawing.Size(24, 19);
            this.lblRequester.TabIndex = 11;
            this.lblRequester.Text = "SP";
            this.lblRequester.WrapToLine = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(20, 148);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(67, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Requester";
            this.metroLabel6.WrapToLine = true;
            // 
            // lblSponser
            // 
            this.lblSponser.AutoSize = true;
            this.lblSponser.Location = new System.Drawing.Point(125, 118);
            this.lblSponser.Name = "lblSponser";
            this.lblSponser.Size = new System.Drawing.Size(24, 19);
            this.lblSponser.TabIndex = 9;
            this.lblSponser.Text = "SP";
            this.lblSponser.WrapToLine = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(20, 113);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(56, 19);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "Sponser";
            this.metroLabel5.WrapToLine = true;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(125, 84);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(90, 19);
            this.lblEndDate.TabIndex = 7;
            this.lblEndDate.Text = "Project Name";
            this.lblEndDate.WrapToLine = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(20, 79);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(62, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "End Date";
            this.metroLabel4.WrapToLine = true;
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(125, 50);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(90, 19);
            this.lblStartDate.TabIndex = 5;
            this.lblStartDate.Text = "Project Name";
            this.lblStartDate.WrapToLine = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(20, 45);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(64, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "StartDate";
            this.metroLabel3.WrapToLine = true;
            // 
            // lblProjectName
            // 
            this.lblProjectName.AutoSize = true;
            this.lblProjectName.Location = new System.Drawing.Point(125, 21);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(90, 19);
            this.lblProjectName.TabIndex = 3;
            this.lblProjectName.Text = "Project Name";
            this.lblProjectName.WrapToLine = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(20, 16);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Project Name";
            this.metroLabel1.WrapToLine = true;
            // 
            // txtDetails
            // 
            // 
            // 
            // 
            this.txtDetails.CustomButton.Image = null;
            this.txtDetails.CustomButton.Location = new System.Drawing.Point(559, 1);
            this.txtDetails.CustomButton.Name = "";
            this.txtDetails.CustomButton.Size = new System.Drawing.Size(81, 81);
            this.txtDetails.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDetails.CustomButton.TabIndex = 1;
            this.txtDetails.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDetails.CustomButton.UseSelectable = true;
            this.txtDetails.CustomButton.Visible = false;
            this.txtDetails.Lines = new string[0];
            this.txtDetails.Location = new System.Drawing.Point(125, 186);
            this.txtDetails.MaxLength = 32767;
            this.txtDetails.Multiline = true;
            this.txtDetails.Name = "txtDetails";
            this.txtDetails.PasswordChar = '\0';
            this.txtDetails.ReadOnly = true;
            this.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDetails.SelectedText = "";
            this.txtDetails.SelectionLength = 0;
            this.txtDetails.SelectionStart = 0;
            this.txtDetails.ShortcutsEnabled = true;
            this.txtDetails.Size = new System.Drawing.Size(641, 83);
            this.txtDetails.TabIndex = 27;
            this.txtDetails.UseSelectable = true;
            this.txtDetails.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDetails.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPurpose
            // 
            // 
            // 
            // 
            this.txtPurpose.CustomButton.Image = null;
            this.txtPurpose.CustomButton.Location = new System.Drawing.Point(559, 1);
            this.txtPurpose.CustomButton.Name = "";
            this.txtPurpose.CustomButton.Size = new System.Drawing.Size(81, 81);
            this.txtPurpose.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPurpose.CustomButton.TabIndex = 1;
            this.txtPurpose.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPurpose.CustomButton.UseSelectable = true;
            this.txtPurpose.CustomButton.Visible = false;
            this.txtPurpose.Lines = new string[0];
            this.txtPurpose.Location = new System.Drawing.Point(125, 275);
            this.txtPurpose.MaxLength = 32767;
            this.txtPurpose.Multiline = true;
            this.txtPurpose.Name = "txtPurpose";
            this.txtPurpose.PasswordChar = '\0';
            this.txtPurpose.ReadOnly = true;
            this.txtPurpose.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPurpose.SelectedText = "";
            this.txtPurpose.SelectionLength = 0;
            this.txtPurpose.SelectionStart = 0;
            this.txtPurpose.ShortcutsEnabled = true;
            this.txtPurpose.Size = new System.Drawing.Size(641, 83);
            this.txtPurpose.TabIndex = 28;
            this.txtPurpose.UseSelectable = true;
            this.txtPurpose.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPurpose.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtScope
            // 
            // 
            // 
            // 
            this.txtScope.CustomButton.Image = null;
            this.txtScope.CustomButton.Location = new System.Drawing.Point(559, 1);
            this.txtScope.CustomButton.Name = "";
            this.txtScope.CustomButton.Size = new System.Drawing.Size(81, 81);
            this.txtScope.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtScope.CustomButton.TabIndex = 1;
            this.txtScope.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtScope.CustomButton.UseSelectable = true;
            this.txtScope.CustomButton.Visible = false;
            this.txtScope.Lines = new string[0];
            this.txtScope.Location = new System.Drawing.Point(125, 364);
            this.txtScope.MaxLength = 32767;
            this.txtScope.Multiline = true;
            this.txtScope.Name = "txtScope";
            this.txtScope.PasswordChar = '\0';
            this.txtScope.ReadOnly = true;
            this.txtScope.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtScope.SelectedText = "";
            this.txtScope.SelectionLength = 0;
            this.txtScope.SelectionStart = 0;
            this.txtScope.ShortcutsEnabled = true;
            this.txtScope.Size = new System.Drawing.Size(641, 83);
            this.txtScope.TabIndex = 29;
            this.txtScope.UseSelectable = true;
            this.txtScope.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtScope.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtStackholder
            // 
            // 
            // 
            // 
            this.txtStackholder.CustomButton.Image = null;
            this.txtStackholder.CustomButton.Location = new System.Drawing.Point(593, 2);
            this.txtStackholder.CustomButton.Name = "";
            this.txtStackholder.CustomButton.Size = new System.Drawing.Size(45, 45);
            this.txtStackholder.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtStackholder.CustomButton.TabIndex = 1;
            this.txtStackholder.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtStackholder.CustomButton.UseSelectable = true;
            this.txtStackholder.CustomButton.Visible = false;
            this.txtStackholder.Lines = new string[0];
            this.txtStackholder.Location = new System.Drawing.Point(125, 461);
            this.txtStackholder.MaxLength = 32767;
            this.txtStackholder.Multiline = true;
            this.txtStackholder.Name = "txtStackholder";
            this.txtStackholder.PasswordChar = '\0';
            this.txtStackholder.ReadOnly = true;
            this.txtStackholder.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStackholder.SelectedText = "";
            this.txtStackholder.SelectionLength = 0;
            this.txtStackholder.SelectionStart = 0;
            this.txtStackholder.ShortcutsEnabled = true;
            this.txtStackholder.Size = new System.Drawing.Size(641, 50);
            this.txtStackholder.TabIndex = 30;
            this.txtStackholder.UseSelectable = true;
            this.txtStackholder.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtStackholder.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // OverviewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.metroPanel1);
            this.Name = "OverviewProject";
            this.Size = new System.Drawing.Size(776, 531);
            this.Load += new System.EventHandler(this.OverviewProject_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblProjectName;
        private MetroFramework.Controls.MetroLabel lblStartDate;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblEndDate;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel lblSponser;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblRequester;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel lblDomain;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel lblType;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel lblAuthor;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel lblManager;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroTextBox txtDetails;
        private MetroFramework.Controls.MetroTextBox txtStackholder;
        private MetroFramework.Controls.MetroTextBox txtScope;
        private MetroFramework.Controls.MetroTextBox txtPurpose;
    }
}
