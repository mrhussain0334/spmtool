﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPMTool.Forms.User;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class UserRequirements : UserControl
    {
        private readonly RequirementRepository _requirementRepository = new RequirementRepository();
        public UserRequirements()
        {
            InitializeComponent();
        }

        private void Reset()
        {
            gridRequirements.DataSource = null;
            gridRequirements.DataSource = _requirementRepository.GetRequirementByUserId(ConfigService.Instance.Id);
            gridRequirements.ReadOnly = true;
            gridRequirements.ScrollBars = ScrollBars.Both;
            gridRequirements.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridRequirements.MultiSelect = false;

        }

        private void btnCompleteRequirement_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Requirement>)gridRequirements.DataSource)[gridRequirements.SelectedRows[0].Index];
                var id = selectedItem.Id;
                if (selectedItem.IsCompleted)
                {
                    MessagBoxService.Show(this,"Already Completed","");
                }
                new CompleteRequirementForm(id).ShowDialog();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
            Reset();
        }

        private void metroPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void UserRequirements_Load(object sender, EventArgs e)
        {
            try
            {
                Reset();
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
