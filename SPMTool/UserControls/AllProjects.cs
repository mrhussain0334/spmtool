﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework;
using SPMTool.Forms.Project;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class AllProjects : UserControl
    {
        
        private ProjectRepository _repo = new ProjectRepository();
        private ProjectCharterRepository _charterRepository = new ProjectCharterRepository();
        private bool _isAdmin;
        public AllProjects(bool isAdmin)
        {
            _isAdmin = isAdmin;
            InitializeComponent();
        }
        private void AllProjects_Load(object sender, EventArgs e)
        {
            try
            {
                ResetGrid();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
        }

        private void btnCreateProject_Click(object sender, EventArgs e)
        {
            try
            {
                new ManageProjectCharterForm(false).ShowDialog();
                ResetGrid();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
        }

        private void btnEditProject_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Project>)gridProjects.DataSource)[gridProjects.SelectedRows[0].Index];
                var id = selectedItem.Id;
                var form = new ManageProjectCharterForm(true, id);
                form.ShowDialog();
            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this, exception.Message);
            }
            ResetGrid();
        }

        private void ResetGrid()
        {
            gridProjects.DataSource = _isAdmin ? _repo.GetAllProjectsByCreater(ConfigService.Instance.Id) : _repo.GetAllProjects();
            gridProjects.ReadOnly = true;
            gridProjects.ScrollBars = ScrollBars.Both;
            gridProjects.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridProjects.MultiSelect = false;
        }

        private void btnViewProject_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Project>)gridProjects.DataSource)[gridProjects.SelectedRows[0].Index];
                var id = selectedItem.Id;
                var charter = _charterRepository.GetProjectCharterByProjectId(id);
                new ViewProject(charter).ShowDialog();
            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this, exception.Message);
            }
            ResetGrid();

        }

        private void btnDeleteProject_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Project>)gridProjects.DataSource)[gridProjects.SelectedRows[0].Index];
                var id = selectedItem.Id;
                
            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this, exception.Message);
            }
            ResetGrid();
        }

        private void btnFinishProject_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Project>)gridProjects.DataSource)[gridProjects.SelectedRows[0].Index];
                var id = selectedItem.Id;

            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this, exception.Message);
            }
            ResetGrid();
        }
    }
}
