﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using OfficeOpenXml;
using SPMTool.Forms.Charts;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class AllRequirements : UserControl
    {
        private readonly ProjectCharter _projectCharter;
        private readonly RequirementRepository _requirementRepository = new RequirementRepository();
        private readonly RequirementTypeRepository _requirementTypeRepository = new RequirementTypeRepository();
        private readonly RequirementDependencyRepository _requirementDependencyRepository = new RequirementDependencyRepository();
        public AllRequirements(ProjectCharter projectCharter)
        {
            _projectCharter = projectCharter;
            InitializeComponent();
        }

        private void metroPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AllRequirements_Load(object sender, EventArgs e)
        {
            try
            {
                ResetGrid();
                ResetComboBox();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ResetGrid()
        {
            gridRequirements.DataSource = _requirementRepository.GetRequirementsByProjectId(_projectCharter.ProjectId);
            gridRequirements.MultiSelect = false;
            gridRequirements.ScrollBars = ScrollBars.Both;
            gridRequirements.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridRequirements.ReadOnly = true;
        }

        private void ResetComboBox()
        {
            cbxDependency.DataSource = _requirementRepository.GetRequirementsByProjectId(_projectCharter.ProjectId);
            cbxDependency.ValueMember = "Id";
            cbxDependency.DisplayMember = "";
        }
        private void btnImportRequirements_Click(object sender, EventArgs e)
        {
            try
            {
                var fileDialog = new OpenFileDialog { Filter = @"Excel Files|*.xls;*.xlsx;*.xlsm", Multiselect = false };
                var result = fileDialog.ShowDialog();
                if (result == DialogResult.OK) // Test result.
                {
                    var dict = new Dictionary<string, string>();
                    var newFile = new FileInfo(fileDialog.FileName);
                    using (var pck = new ExcelPackage(newFile))
                    {
                        //Add the Content sheet
                        var ws = pck.Workbook.Worksheets["Sheet1"];
                        int i = 2;
                        while (true)
                        {
                            var code = ws.Cells["A" + i].Text;
                            var requirement = ws.Cells["B" + i].Text;
                            var deadline = ws.Cells["C" + i].Text;
                            var dependencies = ws.Cells["D" + i].Text;
                            i++;
                            if (string.IsNullOrEmpty(code))
                            {
                                break;
                            }
                            if (!_requirementRepository.CheckIfKeyExist(code, _projectCharter.ProjectId))
                            {
                                var req = new Requirement()
                                {
                                    ProjectId = _projectCharter.ProjectId,
                                    CreatedAt = DateTime.Now,
                                    UpdateAt = DateTime.Now,
                                    Code = code,
                                    Description = requirement,
                                    Deadline = deadline,
                                    RequirementTypeId = 1
                                };
                                _requirementRepository.Add(req);
                                dict.Add(code, dependencies);
                            }
                        }

                        foreach (var key in dict.Keys)
                        {
                            var dependencies = dict[key].Split(',');
                            var r = _requirementRepository.GetRequirementsByCodeByProjectId(key,
                                    _projectCharter.ProjectId);
                            foreach (var dependency in dependencies)
                            {
                                var d = _requirementRepository.GetRequirementsByCodeByProjectId(dependency,
                                    _projectCharter.ProjectId);
                                if (d != null)
                                {
                                    var req = new RequirementDependency()
                                    {
                                        RequirementId = r.Id,
                                        DependencyRequirementId = d.Id,
                                        CreatedAt = DateTime.Now,
                                        UpdateAt = DateTime.Now,
                                    };
                                    _requirementDependencyRepository.Add(req);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
            Clear();
            ResetGrid();
            ResetComboBox();

        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (VerifyAdd())
            {
                MessagBoxService.Show(this, "Requirements Added", "Success");
                Clear();
                ResetGrid();
                ResetComboBox();
            }
        }


        private bool VerifyUpdate()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            if (_requirementRepository.CheckIfKeyExist(txtKey.Text, _projectCharter.ProjectId))
            {
                MessagBoxService.Show(this, "Key Already Exists", "Error");
                return false;
            }
            var requirement = new Requirement()
            {
                Code = txtKey.Text,
                Deadline = txtDuration.Text,
                RequirementTypeId = 1,
                Description = txtRequirment.Text,
                ProjectId = _projectCharter.ProjectId,
                IsCompleted = false,
            };

            _requirementRepository.Add(requirement);
            return true;
        }

        private bool VerifyAdd()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            if (_requirementRepository.CheckIfKeyExist(txtKey.Text, _projectCharter.ProjectId))
            {
                MessagBoxService.Show(this, "Key Already Exists", "Error");
                return false;
            }
            var requirement = new Requirement()
            {
                Code = txtKey.Text,
                Deadline = txtDuration.Text,
                RequirementTypeId = 1,
                Description = txtRequirment.Text,
                ProjectId = _projectCharter.ProjectId,
                IsCompleted = false,
                CreatedAt = DateTime.Now,
                UpdateAt = DateTime.Now,
            };
            _requirementRepository.Add(requirement);
            if (cbIsDependent.Checked)
            {
                var d = _requirementRepository.GetRequirementById(int.Parse(cbxDependency.SelectedValue.ToString()));
                var req = new RequirementDependency()
                {
                    DependencyRequirementId = d.Id,
                    RequirementId = requirement.Id,
                    CreatedAt = DateTime.Now,
                    UpdateAt = DateTime.Now,
                };
                _requirementDependencyRepository.Add(req);
               
            }
            return true;
        }

        private void Clear()
        {
            var list = Controls;
            foreach (Control control in list)
            {
                if (control.GetType() == typeof(MetroTextBox))
                {
                    control.Text = "";
                }
            }
            cbIsDependent.Checked = false;
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Requirement>)gridRequirements.DataSource)[gridRequirements.SelectedRows[0].Index];
                var id = selectedItem.Id;
                if (
                    MessagBoxService.Confirm(this, "Do you want to Delete this Requirement?",
                        "Confirm Deletion") == DialogResult.Yes)
                {
                    new RequirementRepository().DeleteRequirement(id);
                    ResetGrid();
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception.Message, "Error");
            }
        }

        private void btnShowGanttChart_Click(object sender, EventArgs e)
        {
            try
            {
                new GanttChart(_requirementRepository.GetRequirementsByProjectId(_projectCharter.ProjectId)).ShowDialog();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception.Message, "Error");
            }
        }
    }
}
