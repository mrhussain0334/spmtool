﻿namespace SPMTool.UserControls
{
    partial class AllRequirements
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.btnShowGanttChart = new MetroFramework.Controls.MetroButton();
            this.cbIsDependent = new MetroFramework.Controls.MetroCheckBox();
            this.btnClear = new MetroFramework.Controls.MetroButton();
            this.btnDelete = new MetroFramework.Controls.MetroButton();
            this.gridRequirements = new MetroFramework.Controls.MetroGrid();
            this.cbxDependency = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtDuration = new MetroFramework.Controls.MetroTextBox();
            this.btnAdd = new MetroFramework.Controls.MetroButton();
            this.txtRequirment = new MetroFramework.Controls.MetroTextBox();
            this.txtKey = new MetroFramework.Controls.MetroTextBox();
            this.btnImportRequirements = new MetroFramework.Controls.MetroButton();
            this.requirementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.updateAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requirementTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requirementTypeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedToUserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedToUserIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isCompletedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.actualStartDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actualEndDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expectedStartDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expectedEndDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deadlineDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRequirements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requirementBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.metroPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.metroPanel1.Controls.Add(this.btnShowGanttChart);
            this.metroPanel1.Controls.Add(this.cbIsDependent);
            this.metroPanel1.Controls.Add(this.btnClear);
            this.metroPanel1.Controls.Add(this.btnDelete);
            this.metroPanel1.Controls.Add(this.gridRequirements);
            this.metroPanel1.Controls.Add(this.cbxDependency);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.txtDuration);
            this.metroPanel1.Controls.Add(this.btnAdd);
            this.metroPanel1.Controls.Add(this.txtRequirment);
            this.metroPanel1.Controls.Add(this.txtKey);
            this.metroPanel1.Controls.Add(this.btnImportRequirements);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(647, 505);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            this.metroPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.metroPanel1_Paint);
            // 
            // btnShowGanttChart
            // 
            this.btnShowGanttChart.Location = new System.Drawing.Point(158, 44);
            this.btnShowGanttChart.Name = "btnShowGanttChart";
            this.btnShowGanttChart.Size = new System.Drawing.Size(136, 33);
            this.btnShowGanttChart.TabIndex = 26;
            this.btnShowGanttChart.Text = "Show Gantt Chart";
            this.btnShowGanttChart.UseSelectable = true;
            this.btnShowGanttChart.Click += new System.EventHandler(this.btnShowGanttChart_Click);
            // 
            // cbIsDependent
            // 
            this.cbIsDependent.AutoSize = true;
            this.cbIsDependent.Location = new System.Drawing.Point(450, 165);
            this.cbIsDependent.Name = "cbIsDependent";
            this.cbIsDependent.Size = new System.Drawing.Size(92, 15);
            this.cbIsDependent.TabIndex = 25;
            this.cbIsDependent.Text = "Is Dependent";
            this.cbIsDependent.UseSelectable = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(548, 141);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 24;
            this.btnClear.Text = "Clear";
            this.btnClear.UseSelectable = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(548, 112);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(88, 23);
            this.btnDelete.TabIndex = 23;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseSelectable = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // gridRequirements
            // 
            this.gridRequirements.AllowUserToResizeRows = false;
            this.gridRequirements.AutoGenerateColumns = false;
            this.gridRequirements.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridRequirements.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridRequirements.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridRequirements.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridRequirements.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridRequirements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRequirements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codeDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.deadlineDataGridViewTextBoxColumn,
            this.expectedEndDateDataGridViewTextBoxColumn,
            this.expectedStartDateDataGridViewTextBoxColumn,
            this.actualEndDateDataGridViewTextBoxColumn,
            this.actualStartDateDataGridViewTextBoxColumn,
            this.isCompletedDataGridViewCheckBoxColumn,
            this.assignedToUserIdDataGridViewTextBoxColumn,
            this.assignedToUserDataGridViewTextBoxColumn,
            this.requirementTypeIdDataGridViewTextBoxColumn,
            this.requirementTypeDataGridViewTextBoxColumn,
            this.projectIdDataGridViewTextBoxColumn,
            this.projectDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn,
            this.createdAtDataGridViewTextBoxColumn,
            this.updateAtDataGridViewTextBoxColumn});
            this.gridRequirements.DataSource = this.requirementBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridRequirements.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridRequirements.EnableHeadersVisualStyles = false;
            this.gridRequirements.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridRequirements.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridRequirements.Location = new System.Drawing.Point(16, 212);
            this.gridRequirements.Name = "gridRequirements";
            this.gridRequirements.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridRequirements.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridRequirements.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridRequirements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridRequirements.Size = new System.Drawing.Size(620, 278);
            this.gridRequirements.TabIndex = 19;
            // 
            // cbxDependency
            // 
            this.cbxDependency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxDependency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxDependency.FormattingEnabled = true;
            this.cbxDependency.ItemHeight = 23;
            this.cbxDependency.Location = new System.Drawing.Point(107, 151);
            this.cbxDependency.Name = "cbxDependency";
            this.cbxDependency.Size = new System.Drawing.Size(337, 29);
            this.cbxDependency.TabIndex = 18;
            this.cbxDependency.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(16, 151);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(85, 38);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Dependency \r\n(Optional)";
            // 
            // txtDuration
            // 
            // 
            // 
            // 
            this.txtDuration.CustomButton.Image = null;
            this.txtDuration.CustomButton.Location = new System.Drawing.Point(164, 1);
            this.txtDuration.CustomButton.Name = "";
            this.txtDuration.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDuration.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDuration.CustomButton.TabIndex = 1;
            this.txtDuration.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDuration.CustomButton.UseSelectable = true;
            this.txtDuration.CustomButton.Visible = false;
            this.txtDuration.Lines = new string[0];
            this.txtDuration.Location = new System.Drawing.Point(450, 83);
            this.txtDuration.MaxLength = 32767;
            this.txtDuration.Name = "txtDuration";
            this.txtDuration.PasswordChar = '\0';
            this.txtDuration.PromptText = "Enter Days";
            this.txtDuration.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDuration.SelectedText = "";
            this.txtDuration.SelectionLength = 0;
            this.txtDuration.SelectionStart = 0;
            this.txtDuration.ShortcutsEnabled = true;
            this.txtDuration.Size = new System.Drawing.Size(186, 23);
            this.txtDuration.TabIndex = 16;
            this.txtDuration.UseSelectable = true;
            this.txtDuration.WaterMark = "Enter Days";
            this.txtDuration.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDuration.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(450, 112);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(92, 23);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseSelectable = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtRequirment
            // 
            // 
            // 
            // 
            this.txtRequirment.CustomButton.Image = null;
            this.txtRequirment.CustomButton.Location = new System.Drawing.Point(277, 2);
            this.txtRequirment.CustomButton.Name = "";
            this.txtRequirment.CustomButton.Size = new System.Drawing.Size(57, 57);
            this.txtRequirment.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtRequirment.CustomButton.TabIndex = 1;
            this.txtRequirment.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtRequirment.CustomButton.UseSelectable = true;
            this.txtRequirment.CustomButton.Visible = false;
            this.txtRequirment.Lines = new string[0];
            this.txtRequirment.Location = new System.Drawing.Point(107, 83);
            this.txtRequirment.MaxLength = 32767;
            this.txtRequirment.Multiline = true;
            this.txtRequirment.Name = "txtRequirment";
            this.txtRequirment.PasswordChar = '\0';
            this.txtRequirment.PromptText = "Enter Requirment";
            this.txtRequirment.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRequirment.SelectedText = "";
            this.txtRequirment.SelectionLength = 0;
            this.txtRequirment.SelectionStart = 0;
            this.txtRequirment.ShortcutsEnabled = true;
            this.txtRequirment.Size = new System.Drawing.Size(337, 62);
            this.txtRequirment.TabIndex = 14;
            this.txtRequirment.UseSelectable = true;
            this.txtRequirment.WaterMark = "Enter Requirment";
            this.txtRequirment.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtRequirment.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtKey
            // 
            // 
            // 
            // 
            this.txtKey.CustomButton.Image = null;
            this.txtKey.CustomButton.Location = new System.Drawing.Point(67, 1);
            this.txtKey.CustomButton.Name = "";
            this.txtKey.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtKey.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtKey.CustomButton.TabIndex = 1;
            this.txtKey.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtKey.CustomButton.UseSelectable = true;
            this.txtKey.CustomButton.Visible = false;
            this.txtKey.Lines = new string[0];
            this.txtKey.Location = new System.Drawing.Point(16, 83);
            this.txtKey.MaxLength = 32767;
            this.txtKey.Multiline = true;
            this.txtKey.Name = "txtKey";
            this.txtKey.PasswordChar = '\0';
            this.txtKey.PromptText = "Enter Key";
            this.txtKey.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKey.SelectedText = "";
            this.txtKey.SelectionLength = 0;
            this.txtKey.SelectionStart = 0;
            this.txtKey.ShortcutsEnabled = true;
            this.txtKey.Size = new System.Drawing.Size(89, 23);
            this.txtKey.TabIndex = 13;
            this.txtKey.UseSelectable = true;
            this.txtKey.WaterMark = "Enter Key";
            this.txtKey.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtKey.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnImportRequirements
            // 
            this.btnImportRequirements.Location = new System.Drawing.Point(16, 44);
            this.btnImportRequirements.Name = "btnImportRequirements";
            this.btnImportRequirements.Size = new System.Drawing.Size(136, 33);
            this.btnImportRequirements.TabIndex = 12;
            this.btnImportRequirements.Text = "Import Requirements";
            this.btnImportRequirements.UseSelectable = true;
            this.btnImportRequirements.Click += new System.EventHandler(this.btnImportRequirements_Click);
            // 
            // requirementBindingSource
            // 
            this.requirementBindingSource.DataSource = typeof(SPMTool.Models.Requirement);
            // 
            // updateAtDataGridViewTextBoxColumn
            // 
            this.updateAtDataGridViewTextBoxColumn.DataPropertyName = "UpdateAt";
            this.updateAtDataGridViewTextBoxColumn.HeaderText = "UpdateAt";
            this.updateAtDataGridViewTextBoxColumn.Name = "updateAtDataGridViewTextBoxColumn";
            // 
            // createdAtDataGridViewTextBoxColumn
            // 
            this.createdAtDataGridViewTextBoxColumn.DataPropertyName = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.HeaderText = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.Name = "createdAtDataGridViewTextBoxColumn";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // projectDataGridViewTextBoxColumn
            // 
            this.projectDataGridViewTextBoxColumn.DataPropertyName = "Project";
            this.projectDataGridViewTextBoxColumn.HeaderText = "Project";
            this.projectDataGridViewTextBoxColumn.Name = "projectDataGridViewTextBoxColumn";
            // 
            // projectIdDataGridViewTextBoxColumn
            // 
            this.projectIdDataGridViewTextBoxColumn.DataPropertyName = "ProjectId";
            this.projectIdDataGridViewTextBoxColumn.HeaderText = "ProjectId";
            this.projectIdDataGridViewTextBoxColumn.Name = "projectIdDataGridViewTextBoxColumn";
            // 
            // requirementTypeDataGridViewTextBoxColumn
            // 
            this.requirementTypeDataGridViewTextBoxColumn.DataPropertyName = "RequirementType";
            this.requirementTypeDataGridViewTextBoxColumn.HeaderText = "RequirementType";
            this.requirementTypeDataGridViewTextBoxColumn.Name = "requirementTypeDataGridViewTextBoxColumn";
            // 
            // requirementTypeIdDataGridViewTextBoxColumn
            // 
            this.requirementTypeIdDataGridViewTextBoxColumn.DataPropertyName = "RequirementTypeId";
            this.requirementTypeIdDataGridViewTextBoxColumn.HeaderText = "RequirementTypeId";
            this.requirementTypeIdDataGridViewTextBoxColumn.Name = "requirementTypeIdDataGridViewTextBoxColumn";
            // 
            // assignedToUserDataGridViewTextBoxColumn
            // 
            this.assignedToUserDataGridViewTextBoxColumn.DataPropertyName = "AssignedToUser";
            this.assignedToUserDataGridViewTextBoxColumn.HeaderText = "AssignedToUser";
            this.assignedToUserDataGridViewTextBoxColumn.Name = "assignedToUserDataGridViewTextBoxColumn";
            // 
            // assignedToUserIdDataGridViewTextBoxColumn
            // 
            this.assignedToUserIdDataGridViewTextBoxColumn.DataPropertyName = "AssignedToUserId";
            this.assignedToUserIdDataGridViewTextBoxColumn.HeaderText = "AssignedToUserId";
            this.assignedToUserIdDataGridViewTextBoxColumn.Name = "assignedToUserIdDataGridViewTextBoxColumn";
            // 
            // isCompletedDataGridViewCheckBoxColumn
            // 
            this.isCompletedDataGridViewCheckBoxColumn.DataPropertyName = "IsCompleted";
            this.isCompletedDataGridViewCheckBoxColumn.HeaderText = "IsCompleted";
            this.isCompletedDataGridViewCheckBoxColumn.Name = "isCompletedDataGridViewCheckBoxColumn";
            // 
            // actualStartDateDataGridViewTextBoxColumn
            // 
            this.actualStartDateDataGridViewTextBoxColumn.DataPropertyName = "ActualStartDate";
            this.actualStartDateDataGridViewTextBoxColumn.HeaderText = "ActualStartDate";
            this.actualStartDateDataGridViewTextBoxColumn.Name = "actualStartDateDataGridViewTextBoxColumn";
            // 
            // actualEndDateDataGridViewTextBoxColumn
            // 
            this.actualEndDateDataGridViewTextBoxColumn.DataPropertyName = "ActualEndDate";
            this.actualEndDateDataGridViewTextBoxColumn.HeaderText = "ActualEndDate";
            this.actualEndDateDataGridViewTextBoxColumn.Name = "actualEndDateDataGridViewTextBoxColumn";
            // 
            // expectedStartDateDataGridViewTextBoxColumn
            // 
            this.expectedStartDateDataGridViewTextBoxColumn.DataPropertyName = "ExpectedStartDate";
            this.expectedStartDateDataGridViewTextBoxColumn.HeaderText = "ExpectedStartDate";
            this.expectedStartDateDataGridViewTextBoxColumn.Name = "expectedStartDateDataGridViewTextBoxColumn";
            // 
            // expectedEndDateDataGridViewTextBoxColumn
            // 
            this.expectedEndDateDataGridViewTextBoxColumn.DataPropertyName = "ExpectedEndDate";
            this.expectedEndDateDataGridViewTextBoxColumn.HeaderText = "ExpectedEndDate";
            this.expectedEndDateDataGridViewTextBoxColumn.Name = "expectedEndDateDataGridViewTextBoxColumn";
            // 
            // deadlineDataGridViewTextBoxColumn
            // 
            this.deadlineDataGridViewTextBoxColumn.DataPropertyName = "Deadline";
            this.deadlineDataGridViewTextBoxColumn.HeaderText = "Deadline";
            this.deadlineDataGridViewTextBoxColumn.Name = "deadlineDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "Code";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            // 
            // AllRequirements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel1);
            this.Name = "AllRequirements";
            this.Size = new System.Drawing.Size(654, 511);
            this.Load += new System.EventHandler(this.AllRequirements_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRequirements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requirementBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton btnImportRequirements;
        private MetroFramework.Controls.MetroTextBox txtKey;
        private MetroFramework.Controls.MetroTextBox txtRequirment;
        private MetroFramework.Controls.MetroButton btnAdd;
        private MetroFramework.Controls.MetroTextBox txtDuration;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cbxDependency;
        private MetroFramework.Controls.MetroGrid gridRequirements;
        private MetroFramework.Controls.MetroButton btnDelete;
        private MetroFramework.Controls.MetroButton btnClear;
        private MetroFramework.Controls.MetroCheckBox cbIsDependent;
        private MetroFramework.Controls.MetroButton btnShowGanttChart;
        private System.Windows.Forms.BindingSource requirementBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deadlineDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expectedEndDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expectedStartDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actualEndDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actualStartDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isCompletedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedToUserIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedToUserDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn requirementTypeIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn requirementTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn updateAtDataGridViewTextBoxColumn;
    }
}
