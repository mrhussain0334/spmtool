﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework;
using SPMTool.Forms;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class AllAdminProjectManager : UserControl
    {
        public AllAdminProjectManager()
        {
            InitializeComponent();
        }

        private void btnCreateManager_Click(object sender, EventArgs e)
        {
            try
            {
                var form = new ProjectManagerManageForm(false);
                form.ShowDialog(this);
                ResetGridView();
            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this, exception.Message);
            }
        }

        private void AllAdminProjectManager_Load(object sender, EventArgs e)
        {
            ResetGridView();
            projectManagerGrid.ReadOnly = true;
            projectManagerGrid.ScrollBars = ScrollBars.Both;
            projectManagerGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            projectManagerGrid.MultiSelect = false;
        }

        private void ResetGridView()
        {
            var repo = new ProjectManagerRepository();
            var list = repo.GetAllManagers();
            projectManagerGrid.DataSource = list;
        }

        private void btnEditManager_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<ProjectManager>)projectManagerGrid.DataSource)[projectManagerGrid.SelectedRows[0].Index];
                var id = selectedItem.Id;
                var form = new ProjectManagerManageForm(true, id);
                form.ShowDialog();
                ResetGridView();

            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this,exception.Message);
            }
        }

        private void btnDeleteManager_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<ProjectManager>)projectManagerGrid.DataSource)[projectManagerGrid.SelectedRows[0].Index];
                var id = selectedItem.Id;
                if (
                    MessagBoxService.Confirm(this, "Do you want to Delete this Mananger and all its Stuff?",
                        "Confirm Deletion") == DialogResult.Yes)
                {
                    new ProjectManagerRepository().Delete(id);
                    ResetGridView();
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception.Message,"Error");
            }
        }

        private void btnUpdateCredentials_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<ProjectManager>)projectManagerGrid.DataSource)[projectManagerGrid.SelectedRows[0].Index];
                var id = selectedItem.Id;
                new ProjectManagerUpdateCredentialsForm(id).ShowDialog();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception.Message,"Error");
            }
        }

        private void btnViewManager_Click(object sender, EventArgs e)
        {

        }
    }
}
