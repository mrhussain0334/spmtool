﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using OfficeOpenXml;
using SPMTool.Forms.WBS;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class AllWBS : UserControl
    {
        private readonly WbsRepository _wbsRepository = new WbsRepository();
        private readonly ProjectCharter _charter;
        public AllWBS(ProjectCharter charter)
        {
            _charter = charter;
            InitializeComponent();
        }

        private void AllWBS_Load(object sender, EventArgs e)
        {
            try
            {
                ResetGrid();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ResetGrid()
        {
            gridWbs.DataSource = _wbsRepository.GetWbsesByProjectId(_charter.ProjectId);
            gridWbs.ReadOnly = true;
            gridWbs.ScrollBars = ScrollBars.Both;
            gridWbs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridWbs.MultiSelect = false;
        }

        private void Reset()
        {
            txtCode.Text = txtDescription.Text = txtPackage.Text = "";
        }
        private bool VerifyAdd()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }

            if(_wbsRepository.CheckIfExist(txtCode.Text,_charter.ProjectId))
            {
                MessagBoxService.Show(this, "Code Already Exist", "Error");
                return false;
            }
            var wbs = new Wbs()
            {
                ProjectId = _charter.ProjectId,
                Code = txtCode.Text,
                Description = txtDescription.Text,
                CreatedAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                Package = txtPackage.Text,
            };
            _wbsRepository.Add(wbs);
            return true;
        }

        private void btnAddWBS_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerifyAdd())
                {
                    MessagBoxService.Show(this,"Wbs Added","Success");
                    Reset();
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
            ResetGrid();
        }

        private void btnDeleteWBS_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<Wbs>)gridWbs.DataSource)[gridWbs.SelectedRows[0].Index];
                var id = selectedItem.Id;
                if (
                    MessagBoxService.Confirm(this, "Do you want to Delete Wbs?",
                        "Confirm Deletion") == DialogResult.Yes)
                {
                    _wbsRepository.Delete(id);
                    ResetGrid();
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
        }

        private void btnImportWBS_Click(object sender, EventArgs e)
        {
            try
            {
                var fileDialog = new OpenFileDialog { Filter = @"Excel Files|*.xls;*.xlsx;*.xlsm", Multiselect = false };
                var result = fileDialog.ShowDialog();
                if (result == DialogResult.OK) // Test result.
                {
                    var newFile = new FileInfo(fileDialog.FileName);
                    using (var pck = new ExcelPackage(newFile))
                    {
                        //Add the Content sheet
                        var ws = pck.Workbook.Worksheets["Sheet1"];
                        int i = 2;
                        while (true)
                        {
                            var code = ws.Cells["A" + i].Text;
                            var description = ws.Cells["C" + i].Text;
                            var package = ws.Cells["B" + i].Text;
                            if(code == "")
                                break;

                            if (!_wbsRepository.CheckIfExist(code, _charter.ProjectId))
                            {
                                var wbs = new Wbs()
                                {
                                    Code = code,
                                    ProjectId = _charter.ProjectId,
                                    CreatedAt = DateTime.Now,
                                    UpdateAt = DateTime.Now,
                                    Description = description,
                                    Package = package,
                                };

                                _wbsRepository.Add(wbs);
                            }
                            i++;
                        }
                    }
                    MessagBoxService.Show(this,"Completed","Success");
                }
                
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
            ResetGrid();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                new WbsDiagramForm(_charter).ShowDialog();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
        }
    }
}
