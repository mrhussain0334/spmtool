﻿using System;
using System.Windows.Forms;
using MetroFramework.Controls;
using SPMTool.Models;

namespace SPMTool.UserControls
{
    public partial class OverviewProject : MetroUserControl
    {
        private ProjectCharter charter;
        public OverviewProject(ProjectCharter charter)
        {
            this.charter = charter;
            InitializeComponent();
        }

        private void OverviewProject_Load(object sender, EventArgs e)
        {
            metroPanel1.AutoScroll = false;
            metroPanel1.HorizontalScroll.Enabled = false;
            metroPanel1.HorizontalScroll.Visible = false;
            metroPanel1.HorizontalScroll.Maximum = 0;
            metroPanel1.AutoScroll = true;
            lblAuthor.Text = charter.Author;
            txtDetails.Text = charter.Description;
            lblDomain.Text = charter.Project.ProjectDomain.ProjectDomainName;
            lblEndDate.Text = charter.EndDate.ToShortDateString();
            lblManager.Text = charter.Project.ProjectManager.ToString();
            lblProjectName.Text = charter.ProjectName;
            txtPurpose.Text = charter.Purpose;
            lblRequester.Text = charter.Requester;
            txtScope.Text = charter.Scope;
            txtStackholder.Text = charter.StakeHolders;
            lblType.Text = charter.Project.ProjectType.ToString();
        }

        private void metroPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
