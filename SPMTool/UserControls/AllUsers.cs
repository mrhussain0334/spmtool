﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework;
using SPMTool.Forms.User;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class AllUsers : UserControl
    {
        private UserRepository _repo = new UserRepository();
        private readonly bool _isAdmin;
        public AllUsers(bool isAdmin)
        {
            _isAdmin = isAdmin;
            InitializeComponent();
        }

        private void AllUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ResetGrid();
                if (_isAdmin)
                {
                    Controls.Remove(btnCreateUser);
                    Controls.Remove(btnDeleteUser);
                    Controls.Remove(btnEditUser);
                    Controls.Remove(btnUpdateCredentials);
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
        }

        private void ResetGrid()
        {
            gridUsers.DataSource = _repo.GetAllUsersByProjectManagerId();
            gridUsers.ReadOnly = true;
            gridUsers.ScrollBars = ScrollBars.Both;
            gridUsers.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridUsers.MultiSelect = false;
        }
        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            try
            {
                new ManageUser(false).ShowDialog();
                ResetGrid();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
        }

        private void btnEditUser_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<User>)gridUsers.DataSource)[gridUsers.SelectedRows[0].Index];
                var id = selectedItem.Id;
                var form = new ManageUser(true, id);
                form.ShowDialog();

            }
            catch (Exception exception)
            {
                MetroMessageBox.Show(this, exception.Message);
            }
            ResetGrid();
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<User>)gridUsers.DataSource)[gridUsers.SelectedRows[0].Index];
                var id = selectedItem.Id;
                if (
                    MessagBoxService.Confirm(this, "Do you want to Delete this User and all its Stuff?",
                        "Confirm Deletion") == DialogResult.Yes)
                {
                    new UserRepository().Delete(id);
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception.Message, "Error");
            }
            ResetGrid();
        }

        private void btnViewUser_Click(object sender, EventArgs e)
        {

        }

        private void btnUpdateCredentials_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = ((List<User>)gridUsers.DataSource)[gridUsers.SelectedRows[0].Index];
                var id = selectedItem.Id;
                new UserUpdateCredentials(id).ShowDialog();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception.Message, "Error");
            }
            ResetGrid();
        }
    }
}
