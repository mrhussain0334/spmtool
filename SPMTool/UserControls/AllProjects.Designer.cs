﻿namespace SPMTool.UserControls
{
    partial class AllProjects
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridProjects = new MetroFramework.Controls.MetroGrid();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectDomainDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectManagerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updateAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnDeleteProject = new MetroFramework.Controls.MetroButton();
            this.btnViewProject = new MetroFramework.Controls.MetroButton();
            this.btnEditProject = new MetroFramework.Controls.MetroButton();
            this.btnCreateProject = new MetroFramework.Controls.MetroButton();
            this.btnFinishProject = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridProjects
            // 
            this.gridProjects.AllowUserToResizeRows = false;
            this.gridProjects.AutoGenerateColumns = false;
            this.gridProjects.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridProjects.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridProjects.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridProjects.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridProjects.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridProjects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridProjects.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.projectDomainDataGridViewTextBoxColumn,
            this.projectTypeDataGridViewTextBoxColumn,
            this.projectManagerDataGridViewTextBoxColumn,
            this.createdAtDataGridViewTextBoxColumn,
            this.updateAtDataGridViewTextBoxColumn});
            this.gridProjects.DataSource = this.projectBindingSource;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridProjects.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridProjects.EnableHeadersVisualStyles = false;
            this.gridProjects.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridProjects.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridProjects.Location = new System.Drawing.Point(4, 4);
            this.gridProjects.Name = "gridProjects";
            this.gridProjects.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridProjects.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridProjects.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridProjects.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridProjects.Size = new System.Drawing.Size(725, 294);
            this.gridProjects.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // projectDomainDataGridViewTextBoxColumn
            // 
            this.projectDomainDataGridViewTextBoxColumn.DataPropertyName = "ProjectDomain";
            this.projectDomainDataGridViewTextBoxColumn.HeaderText = "ProjectDomain";
            this.projectDomainDataGridViewTextBoxColumn.Name = "projectDomainDataGridViewTextBoxColumn";
            // 
            // projectTypeDataGridViewTextBoxColumn
            // 
            this.projectTypeDataGridViewTextBoxColumn.DataPropertyName = "ProjectType";
            this.projectTypeDataGridViewTextBoxColumn.HeaderText = "ProjectType";
            this.projectTypeDataGridViewTextBoxColumn.Name = "projectTypeDataGridViewTextBoxColumn";
            // 
            // projectManagerDataGridViewTextBoxColumn
            // 
            this.projectManagerDataGridViewTextBoxColumn.DataPropertyName = "ProjectManager";
            this.projectManagerDataGridViewTextBoxColumn.HeaderText = "ProjectManager";
            this.projectManagerDataGridViewTextBoxColumn.Name = "projectManagerDataGridViewTextBoxColumn";
            // 
            // createdAtDataGridViewTextBoxColumn
            // 
            this.createdAtDataGridViewTextBoxColumn.DataPropertyName = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.HeaderText = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.Name = "createdAtDataGridViewTextBoxColumn";
            // 
            // updateAtDataGridViewTextBoxColumn
            // 
            this.updateAtDataGridViewTextBoxColumn.DataPropertyName = "UpdateAt";
            this.updateAtDataGridViewTextBoxColumn.HeaderText = "UpdateAt";
            this.updateAtDataGridViewTextBoxColumn.Name = "updateAtDataGridViewTextBoxColumn";
            // 
            // projectBindingSource
            // 
            this.projectBindingSource.DataSource = typeof(SPMTool.Models.Project);
            // 
            // btnDeleteProject
            // 
            this.btnDeleteProject.Location = new System.Drawing.Point(430, 321);
            this.btnDeleteProject.Name = "btnDeleteProject";
            this.btnDeleteProject.Size = new System.Drawing.Size(136, 33);
            this.btnDeleteProject.TabIndex = 14;
            this.btnDeleteProject.Text = "Delete Project";
            this.btnDeleteProject.UseSelectable = true;
            this.btnDeleteProject.Click += new System.EventHandler(this.btnDeleteProject_Click);
            // 
            // btnViewProject
            // 
            this.btnViewProject.Location = new System.Drawing.Point(288, 321);
            this.btnViewProject.Name = "btnViewProject";
            this.btnViewProject.Size = new System.Drawing.Size(136, 33);
            this.btnViewProject.TabIndex = 13;
            this.btnViewProject.Text = "View Project";
            this.btnViewProject.UseSelectable = true;
            this.btnViewProject.Click += new System.EventHandler(this.btnViewProject_Click);
            // 
            // btnEditProject
            // 
            this.btnEditProject.Location = new System.Drawing.Point(146, 321);
            this.btnEditProject.Name = "btnEditProject";
            this.btnEditProject.Size = new System.Drawing.Size(136, 33);
            this.btnEditProject.TabIndex = 12;
            this.btnEditProject.Text = "Edit Project";
            this.btnEditProject.UseSelectable = true;
            this.btnEditProject.Click += new System.EventHandler(this.btnEditProject_Click);
            // 
            // btnCreateProject
            // 
            this.btnCreateProject.Location = new System.Drawing.Point(4, 321);
            this.btnCreateProject.Name = "btnCreateProject";
            this.btnCreateProject.Size = new System.Drawing.Size(136, 33);
            this.btnCreateProject.TabIndex = 11;
            this.btnCreateProject.Text = "Create Project";
            this.btnCreateProject.UseSelectable = true;
            this.btnCreateProject.Click += new System.EventHandler(this.btnCreateProject_Click);
            // 
            // btnFinishProject
            // 
            this.btnFinishProject.Location = new System.Drawing.Point(572, 321);
            this.btnFinishProject.Name = "btnFinishProject";
            this.btnFinishProject.Size = new System.Drawing.Size(136, 33);
            this.btnFinishProject.TabIndex = 15;
            this.btnFinishProject.Text = "Finish Project";
            this.btnFinishProject.UseSelectable = true;
            this.btnFinishProject.Click += new System.EventHandler(this.btnFinishProject_Click);
            // 
            // AllProjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnFinishProject);
            this.Controls.Add(this.btnDeleteProject);
            this.Controls.Add(this.btnViewProject);
            this.Controls.Add(this.btnEditProject);
            this.Controls.Add(this.btnCreateProject);
            this.Controls.Add(this.gridProjects);
            this.Name = "AllProjects";
            this.Size = new System.Drawing.Size(732, 430);
            this.Load += new System.EventHandler(this.AllProjects_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroGrid gridProjects;
        private MetroFramework.Controls.MetroButton btnDeleteProject;
        private MetroFramework.Controls.MetroButton btnViewProject;
        private MetroFramework.Controls.MetroButton btnEditProject;
        private MetroFramework.Controls.MetroButton btnCreateProject;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDomainDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectManagerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn updateAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource projectBindingSource;
        private MetroFramework.Controls.MetroButton btnFinishProject;
    }
}
