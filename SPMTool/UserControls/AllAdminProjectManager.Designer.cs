﻿namespace SPMTool.UserControls
{
    partial class AllAdminProjectManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.projectManagerGrid = new MetroFramework.Controls.MetroGrid();
            this.usernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contactNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qualificationsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnCreateManager = new MetroFramework.Controls.MetroButton();
            this.btnEditManager = new MetroFramework.Controls.MetroButton();
            this.btnViewManager = new MetroFramework.Controls.MetroButton();
            this.btnDeleteManager = new MetroFramework.Controls.MetroButton();
            this.btnUpdateCredentials = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.projectManagerGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectManagerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // projectManagerGrid
            // 
            this.projectManagerGrid.AllowUserToResizeRows = false;
            this.projectManagerGrid.AutoGenerateColumns = false;
            this.projectManagerGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.projectManagerGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projectManagerGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.projectManagerGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.projectManagerGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.projectManagerGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.projectManagerGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.usernameDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.firstNameDataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.genderDataGridViewTextBoxColumn,
            this.contactNumberDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.qualificationsDataGridViewTextBoxColumn});
            this.projectManagerGrid.DataSource = this.projectManagerBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.projectManagerGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.projectManagerGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.projectManagerGrid.EnableHeadersVisualStyles = false;
            this.projectManagerGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.projectManagerGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.projectManagerGrid.Location = new System.Drawing.Point(0, 0);
            this.projectManagerGrid.Name = "projectManagerGrid";
            this.projectManagerGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.projectManagerGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.projectManagerGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.projectManagerGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.projectManagerGrid.Size = new System.Drawing.Size(862, 289);
            this.projectManagerGrid.TabIndex = 0;
            // 
            // usernameDataGridViewTextBoxColumn
            // 
            this.usernameDataGridViewTextBoxColumn.DataPropertyName = "Username";
            this.usernameDataGridViewTextBoxColumn.HeaderText = "Username";
            this.usernameDataGridViewTextBoxColumn.Name = "usernameDataGridViewTextBoxColumn";
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "FirstName";
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "FirstName";
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "LastName";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            // 
            // genderDataGridViewTextBoxColumn
            // 
            this.genderDataGridViewTextBoxColumn.DataPropertyName = "Gender";
            this.genderDataGridViewTextBoxColumn.HeaderText = "Gender";
            this.genderDataGridViewTextBoxColumn.Name = "genderDataGridViewTextBoxColumn";
            // 
            // contactNumberDataGridViewTextBoxColumn
            // 
            this.contactNumberDataGridViewTextBoxColumn.DataPropertyName = "ContactNumber";
            this.contactNumberDataGridViewTextBoxColumn.HeaderText = "ContactNumber";
            this.contactNumberDataGridViewTextBoxColumn.Name = "contactNumberDataGridViewTextBoxColumn";
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Address";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            // 
            // qualificationsDataGridViewTextBoxColumn
            // 
            this.qualificationsDataGridViewTextBoxColumn.DataPropertyName = "Qualifications";
            this.qualificationsDataGridViewTextBoxColumn.HeaderText = "Qualifications";
            this.qualificationsDataGridViewTextBoxColumn.Name = "qualificationsDataGridViewTextBoxColumn";
            // 
            // projectManagerBindingSource
            // 
            this.projectManagerBindingSource.DataSource = typeof(SPMTool.Models.ProjectManager);
            // 
            // btnCreateManager
            // 
            this.btnCreateManager.Location = new System.Drawing.Point(3, 295);
            this.btnCreateManager.Name = "btnCreateManager";
            this.btnCreateManager.Size = new System.Drawing.Size(136, 33);
            this.btnCreateManager.TabIndex = 1;
            this.btnCreateManager.Text = "Create Manager";
            this.btnCreateManager.UseSelectable = true;
            this.btnCreateManager.Click += new System.EventHandler(this.btnCreateManager_Click);
            // 
            // btnEditManager
            // 
            this.btnEditManager.Location = new System.Drawing.Point(145, 295);
            this.btnEditManager.Name = "btnEditManager";
            this.btnEditManager.Size = new System.Drawing.Size(136, 33);
            this.btnEditManager.TabIndex = 2;
            this.btnEditManager.Text = "Edit Manager";
            this.btnEditManager.UseSelectable = true;
            this.btnEditManager.Click += new System.EventHandler(this.btnEditManager_Click);
            // 
            // btnViewManager
            // 
            this.btnViewManager.Location = new System.Drawing.Point(287, 295);
            this.btnViewManager.Name = "btnViewManager";
            this.btnViewManager.Size = new System.Drawing.Size(136, 33);
            this.btnViewManager.TabIndex = 3;
            this.btnViewManager.Text = "View Manager";
            this.btnViewManager.UseSelectable = true;
            this.btnViewManager.Click += new System.EventHandler(this.btnViewManager_Click);
            // 
            // btnDeleteManager
            // 
            this.btnDeleteManager.Location = new System.Drawing.Point(429, 295);
            this.btnDeleteManager.Name = "btnDeleteManager";
            this.btnDeleteManager.Size = new System.Drawing.Size(136, 33);
            this.btnDeleteManager.TabIndex = 4;
            this.btnDeleteManager.Text = "Delete Manager";
            this.btnDeleteManager.UseSelectable = true;
            this.btnDeleteManager.Click += new System.EventHandler(this.btnDeleteManager_Click);
            // 
            // btnUpdateCredentials
            // 
            this.btnUpdateCredentials.Location = new System.Drawing.Point(571, 295);
            this.btnUpdateCredentials.Name = "btnUpdateCredentials";
            this.btnUpdateCredentials.Size = new System.Drawing.Size(136, 33);
            this.btnUpdateCredentials.TabIndex = 5;
            this.btnUpdateCredentials.Text = "Update Credentials";
            this.btnUpdateCredentials.UseSelectable = true;
            this.btnUpdateCredentials.Click += new System.EventHandler(this.btnUpdateCredentials_Click);
            // 
            // AllAdminProjectManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnUpdateCredentials);
            this.Controls.Add(this.btnDeleteManager);
            this.Controls.Add(this.btnViewManager);
            this.Controls.Add(this.btnEditManager);
            this.Controls.Add(this.btnCreateManager);
            this.Controls.Add(this.projectManagerGrid);
            this.Name = "AllAdminProjectManager";
            this.Size = new System.Drawing.Size(862, 390);
            this.Load += new System.EventHandler(this.AllAdminProjectManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.projectManagerGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectManagerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroGrid projectManagerGrid;
        private MetroFramework.Controls.MetroButton btnCreateManager;
        private MetroFramework.Controls.MetroButton btnEditManager;
        private MetroFramework.Controls.MetroButton btnViewManager;
        private MetroFramework.Controls.MetroButton btnDeleteManager;
        private MetroFramework.Controls.MetroButton btnUpdateCredentials;
        private System.Windows.Forms.DataGridViewTextBoxColumn usernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contactNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qualificationsDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource projectManagerBindingSource;
    }
}
