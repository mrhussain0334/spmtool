﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.UserControls
{
    public partial class AssignUser : UserControl
    {
        private readonly UserRepository _userRepository = new UserRepository();
        private readonly RequirementRepository _requirementRepository = new RequirementRepository();
        private int _projectId;
        public AssignUser(int projectId)
        {
            _projectId = projectId;
            InitializeComponent();
        }

        private void AssignUser_Load(object sender, EventArgs e)
        {
            try
            {
                Reset();
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        private void Reset()
        {
            cbxUsers.DataSource = _userRepository.GetAllUsersByProjectManagerId(ConfigService.Instance.Id);
            cbxUsers.ValueMember = "Id";
            cbxUsers.DisplayMember = "";
            cbxRequirements.DataSource = _requirementRepository.GetRequirementsByProjectId(_projectId).Where(p => !p.IsCompleted).ToList();
            cbxRequirements.ValueMember = "Id";
            cbxRequirements.DisplayMember = "";
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                var rid = int.Parse(cbxRequirements.SelectedValue.ToString());
                var uid = int.Parse(cbxUsers.SelectedValue.ToString());
                var r = _requirementRepository.GetRequirementById(rid);
                r.AssignedToUserId = uid;
                _requirementRepository.Update(r);
                MessagBoxService.Show(this,"Assigned User","Success");
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
        }
    }
}
