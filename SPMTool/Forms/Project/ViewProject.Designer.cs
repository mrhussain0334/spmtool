﻿namespace SPMTool.Forms.Project
{
    partial class ViewProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewProject));
            this.parentPanel = new MetroFramework.Controls.MetroPanel();
            this.optionPanel = new MetroFramework.Controls.MetroPanel();
            this.btnWbs = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRequirements = new System.Windows.Forms.Button();
            this.metroTile = new System.Windows.Forms.Panel();
            this.btnOverview = new System.Windows.Forms.Button();
            this.btnAssignUser = new System.Windows.Forms.Button();
            this.optionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // parentPanel
            // 
            this.parentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parentPanel.HorizontalScrollbarBarColor = true;
            this.parentPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.parentPanel.HorizontalScrollbarSize = 10;
            this.parentPanel.Location = new System.Drawing.Point(194, 60);
            this.parentPanel.Name = "parentPanel";
            this.parentPanel.Size = new System.Drawing.Size(739, 450);
            this.parentPanel.TabIndex = 5;
            this.parentPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.parentPanel.VerticalScrollbarBarColor = true;
            this.parentPanel.VerticalScrollbarHighlightOnWheel = false;
            this.parentPanel.VerticalScrollbarSize = 10;
            // 
            // optionPanel
            // 
            this.optionPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.optionPanel.Controls.Add(this.btnAssignUser);
            this.optionPanel.Controls.Add(this.btnWbs);
            this.optionPanel.Controls.Add(this.btnClose);
            this.optionPanel.Controls.Add(this.btnRequirements);
            this.optionPanel.Controls.Add(this.metroTile);
            this.optionPanel.Controls.Add(this.btnOverview);
            this.optionPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.optionPanel.HorizontalScrollbarBarColor = true;
            this.optionPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.optionPanel.HorizontalScrollbarSize = 10;
            this.optionPanel.Location = new System.Drawing.Point(20, 60);
            this.optionPanel.Name = "optionPanel";
            this.optionPanel.Size = new System.Drawing.Size(174, 450);
            this.optionPanel.TabIndex = 4;
            this.optionPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.optionPanel.VerticalScrollbarBarColor = true;
            this.optionPanel.VerticalScrollbarHighlightOnWheel = false;
            this.optionPanel.VerticalScrollbarSize = 10;
            // 
            // btnWbs
            // 
            this.btnWbs.BackColor = System.Drawing.Color.White;
            this.btnWbs.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnWbs.FlatAppearance.BorderSize = 0;
            this.btnWbs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnWbs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnWbs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWbs.ForeColor = System.Drawing.Color.Black;
            this.btnWbs.Image = ((System.Drawing.Image)(resources.GetObject("btnWbs.Image")));
            this.btnWbs.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWbs.Location = new System.Drawing.Point(18, 107);
            this.btnWbs.Name = "btnWbs";
            this.btnWbs.Size = new System.Drawing.Size(156, 41);
            this.btnWbs.TabIndex = 6;
            this.btnWbs.Text = "    WBS";
            this.btnWbs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnWbs.UseVisualStyleBackColor = false;
            this.btnWbs.Click += new System.EventHandler(this.btnWbs_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(15, 201);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(156, 41);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "    Close";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // btnRequirements
            // 
            this.btnRequirements.BackColor = System.Drawing.Color.White;
            this.btnRequirements.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRequirements.FlatAppearance.BorderSize = 0;
            this.btnRequirements.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnRequirements.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnRequirements.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRequirements.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRequirements.ForeColor = System.Drawing.Color.Black;
            this.btnRequirements.Image = ((System.Drawing.Image)(resources.GetObject("btnRequirements.Image")));
            this.btnRequirements.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRequirements.Location = new System.Drawing.Point(15, 60);
            this.btnRequirements.Name = "btnRequirements";
            this.btnRequirements.Size = new System.Drawing.Size(156, 41);
            this.btnRequirements.TabIndex = 4;
            this.btnRequirements.Text = "    Requirements";
            this.btnRequirements.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRequirements.UseVisualStyleBackColor = false;
            this.btnRequirements.Click += new System.EventHandler(this.btnRequirements_Click);
            // 
            // metroTile
            // 
            this.metroTile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(225)))));
            this.metroTile.Location = new System.Drawing.Point(3, 3);
            this.metroTile.Name = "metroTile";
            this.metroTile.Size = new System.Drawing.Size(10, 51);
            this.metroTile.TabIndex = 2;
            // 
            // btnOverview
            // 
            this.btnOverview.BackColor = System.Drawing.Color.White;
            this.btnOverview.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnOverview.FlatAppearance.BorderSize = 0;
            this.btnOverview.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnOverview.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnOverview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOverview.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOverview.ForeColor = System.Drawing.Color.Black;
            this.btnOverview.Image = ((System.Drawing.Image)(resources.GetObject("btnOverview.Image")));
            this.btnOverview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOverview.Location = new System.Drawing.Point(15, 3);
            this.btnOverview.Name = "btnOverview";
            this.btnOverview.Size = new System.Drawing.Size(156, 51);
            this.btnOverview.TabIndex = 3;
            this.btnOverview.Text = "    Overview";
            this.btnOverview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOverview.UseVisualStyleBackColor = false;
            this.btnOverview.Click += new System.EventHandler(this.btnOverview_Click);
            // 
            // btnAssignUser
            // 
            this.btnAssignUser.BackColor = System.Drawing.Color.White;
            this.btnAssignUser.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAssignUser.FlatAppearance.BorderSize = 0;
            this.btnAssignUser.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnAssignUser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnAssignUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssignUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssignUser.ForeColor = System.Drawing.Color.Black;
            this.btnAssignUser.Image = ((System.Drawing.Image)(resources.GetObject("btnAssignUser.Image")));
            this.btnAssignUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAssignUser.Location = new System.Drawing.Point(12, 154);
            this.btnAssignUser.Name = "btnAssignUser";
            this.btnAssignUser.Size = new System.Drawing.Size(156, 41);
            this.btnAssignUser.TabIndex = 7;
            this.btnAssignUser.Text = "    User Assign";
            this.btnAssignUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAssignUser.UseVisualStyleBackColor = false;
            this.btnAssignUser.Click += new System.EventHandler(this.btnAssignUser_Click);
            // 
            // ViewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 530);
            this.Controls.Add(this.parentPanel);
            this.Controls.Add(this.optionPanel);
            this.Name = "ViewProject";
            this.Text = "ViewProject";
            this.Load += new System.EventHandler(this.ViewProject_Load);
            this.optionPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel parentPanel;
        private MetroFramework.Controls.MetroPanel optionPanel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRequirements;
        private System.Windows.Forms.Panel metroTile;
        private System.Windows.Forms.Button btnOverview;
        private System.Windows.Forms.Button btnWbs;
        private System.Windows.Forms.Button btnAssignUser;
    }
}