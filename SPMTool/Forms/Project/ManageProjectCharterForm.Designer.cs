﻿namespace SPMTool.Forms.Project
{
    partial class ManageProjectCharterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.dpEndDate = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dpStartDate = new MetroFramework.Controls.MetroDateTime();
            this.txtProjectSponser = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectManagerName = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectRequester = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectAuthor = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectName = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.cbxDomainType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.cbxProjectType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtProjectPurpose = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectDetails = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.txtProjectScope = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectGoals = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.txtProjectRisk = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectStackholder = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.txtProjectAssumptions = new MetroFramework.Controls.MetroTextBox();
            this.txtProjectBudgetRequirement = new MetroFramework.Controls.MetroTextBox();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.btnPrev = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnImport = new MetroFramework.Controls.MetroButton();
            this.tabControl.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            this.metroTabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.metroTabPage1);
            this.tabControl.Controls.Add(this.metroTabPage2);
            this.tabControl.Controls.Add(this.metroTabPage3);
            this.tabControl.Controls.Add(this.metroTabPage4);
            this.tabControl.Controls.Add(this.metroTabPage5);
            this.tabControl.Location = new System.Drawing.Point(23, 63);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(631, 340);
            this.tabControl.TabIndex = 8;
            this.tabControl.UseSelectable = true;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroPanel1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(623, 298);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Project Overview";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.dpEndDate);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.dpStartDate);
            this.metroPanel1.Controls.Add(this.txtProjectSponser);
            this.metroPanel1.Controls.Add(this.txtProjectManagerName);
            this.metroPanel1.Controls.Add(this.txtProjectRequester);
            this.metroPanel1.Controls.Add(this.txtProjectAuthor);
            this.metroPanel1.Controls.Add(this.txtProjectName);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(623, 298);
            this.metroPanel1.TabIndex = 2;
            this.metroPanel1.VerticalScrollbar = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(13, 213);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(123, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Proposed End Date";
            // 
            // dpEndDate
            // 
            this.dpEndDate.Location = new System.Drawing.Point(13, 235);
            this.dpEndDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(600, 29);
            this.dpEndDate.TabIndex = 9;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(13, 159);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 19);
            this.metroLabel1.TabIndex = 8;
            this.metroLabel1.Text = "Expected Start Date";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Location = new System.Drawing.Point(13, 181);
            this.dpStartDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(600, 29);
            this.dpStartDate.TabIndex = 7;
            // 
            // txtProjectSponser
            // 
            // 
            // 
            // 
            this.txtProjectSponser.CustomButton.Image = null;
            this.txtProjectSponser.CustomButton.Location = new System.Drawing.Point(578, 1);
            this.txtProjectSponser.CustomButton.Name = "";
            this.txtProjectSponser.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProjectSponser.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectSponser.CustomButton.TabIndex = 1;
            this.txtProjectSponser.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectSponser.CustomButton.UseSelectable = true;
            this.txtProjectSponser.CustomButton.Visible = false;
            this.txtProjectSponser.Lines = new string[0];
            this.txtProjectSponser.Location = new System.Drawing.Point(13, 129);
            this.txtProjectSponser.MaxLength = 32767;
            this.txtProjectSponser.Name = "txtProjectSponser";
            this.txtProjectSponser.PasswordChar = '\0';
            this.txtProjectSponser.PromptText = "Enter Project Sponser Name";
            this.txtProjectSponser.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProjectSponser.SelectedText = "";
            this.txtProjectSponser.SelectionLength = 0;
            this.txtProjectSponser.SelectionStart = 0;
            this.txtProjectSponser.ShortcutsEnabled = true;
            this.txtProjectSponser.Size = new System.Drawing.Size(600, 23);
            this.txtProjectSponser.TabIndex = 6;
            this.txtProjectSponser.UseSelectable = true;
            this.txtProjectSponser.WaterMark = "Enter Project Sponser Name";
            this.txtProjectSponser.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectSponser.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectManagerName
            // 
            // 
            // 
            // 
            this.txtProjectManagerName.CustomButton.Image = null;
            this.txtProjectManagerName.CustomButton.Location = new System.Drawing.Point(578, 1);
            this.txtProjectManagerName.CustomButton.Name = "";
            this.txtProjectManagerName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProjectManagerName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectManagerName.CustomButton.TabIndex = 1;
            this.txtProjectManagerName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectManagerName.CustomButton.UseSelectable = true;
            this.txtProjectManagerName.CustomButton.Visible = false;
            this.txtProjectManagerName.Lines = new string[0];
            this.txtProjectManagerName.Location = new System.Drawing.Point(13, 100);
            this.txtProjectManagerName.MaxLength = 32767;
            this.txtProjectManagerName.Name = "txtProjectManagerName";
            this.txtProjectManagerName.PasswordChar = '\0';
            this.txtProjectManagerName.PromptText = "Enter Project Manager Name";
            this.txtProjectManagerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProjectManagerName.SelectedText = "";
            this.txtProjectManagerName.SelectionLength = 0;
            this.txtProjectManagerName.SelectionStart = 0;
            this.txtProjectManagerName.ShortcutsEnabled = true;
            this.txtProjectManagerName.Size = new System.Drawing.Size(600, 23);
            this.txtProjectManagerName.TabIndex = 5;
            this.txtProjectManagerName.UseSelectable = true;
            this.txtProjectManagerName.WaterMark = "Enter Project Manager Name";
            this.txtProjectManagerName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectManagerName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectRequester
            // 
            // 
            // 
            // 
            this.txtProjectRequester.CustomButton.Image = null;
            this.txtProjectRequester.CustomButton.Location = new System.Drawing.Point(578, 1);
            this.txtProjectRequester.CustomButton.Name = "";
            this.txtProjectRequester.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProjectRequester.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectRequester.CustomButton.TabIndex = 1;
            this.txtProjectRequester.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectRequester.CustomButton.UseSelectable = true;
            this.txtProjectRequester.CustomButton.Visible = false;
            this.txtProjectRequester.Lines = new string[0];
            this.txtProjectRequester.Location = new System.Drawing.Point(13, 71);
            this.txtProjectRequester.MaxLength = 32767;
            this.txtProjectRequester.Name = "txtProjectRequester";
            this.txtProjectRequester.PasswordChar = '\0';
            this.txtProjectRequester.PromptText = "Enter Project Requester Name";
            this.txtProjectRequester.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProjectRequester.SelectedText = "";
            this.txtProjectRequester.SelectionLength = 0;
            this.txtProjectRequester.SelectionStart = 0;
            this.txtProjectRequester.ShortcutsEnabled = true;
            this.txtProjectRequester.Size = new System.Drawing.Size(600, 23);
            this.txtProjectRequester.TabIndex = 4;
            this.txtProjectRequester.UseSelectable = true;
            this.txtProjectRequester.WaterMark = "Enter Project Requester Name";
            this.txtProjectRequester.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectRequester.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectAuthor
            // 
            // 
            // 
            // 
            this.txtProjectAuthor.CustomButton.Image = null;
            this.txtProjectAuthor.CustomButton.Location = new System.Drawing.Point(578, 1);
            this.txtProjectAuthor.CustomButton.Name = "";
            this.txtProjectAuthor.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProjectAuthor.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectAuthor.CustomButton.TabIndex = 1;
            this.txtProjectAuthor.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectAuthor.CustomButton.UseSelectable = true;
            this.txtProjectAuthor.CustomButton.Visible = false;
            this.txtProjectAuthor.Lines = new string[0];
            this.txtProjectAuthor.Location = new System.Drawing.Point(13, 42);
            this.txtProjectAuthor.MaxLength = 32767;
            this.txtProjectAuthor.Name = "txtProjectAuthor";
            this.txtProjectAuthor.PasswordChar = '\0';
            this.txtProjectAuthor.PromptText = "Enter Project Author Name";
            this.txtProjectAuthor.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProjectAuthor.SelectedText = "";
            this.txtProjectAuthor.SelectionLength = 0;
            this.txtProjectAuthor.SelectionStart = 0;
            this.txtProjectAuthor.ShortcutsEnabled = true;
            this.txtProjectAuthor.Size = new System.Drawing.Size(600, 23);
            this.txtProjectAuthor.TabIndex = 3;
            this.txtProjectAuthor.UseSelectable = true;
            this.txtProjectAuthor.WaterMark = "Enter Project Author Name";
            this.txtProjectAuthor.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectAuthor.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectName
            // 
            // 
            // 
            // 
            this.txtProjectName.CustomButton.Image = null;
            this.txtProjectName.CustomButton.Location = new System.Drawing.Point(578, 1);
            this.txtProjectName.CustomButton.Name = "";
            this.txtProjectName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProjectName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectName.CustomButton.TabIndex = 1;
            this.txtProjectName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectName.CustomButton.UseSelectable = true;
            this.txtProjectName.CustomButton.Visible = false;
            this.txtProjectName.Lines = new string[0];
            this.txtProjectName.Location = new System.Drawing.Point(13, 13);
            this.txtProjectName.MaxLength = 32767;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.PasswordChar = '\0';
            this.txtProjectName.PromptText = "Enter Project Name";
            this.txtProjectName.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectName.SelectedText = "";
            this.txtProjectName.SelectionLength = 0;
            this.txtProjectName.SelectionStart = 0;
            this.txtProjectName.ShortcutsEnabled = true;
            this.txtProjectName.Size = new System.Drawing.Size(600, 23);
            this.txtProjectName.TabIndex = 2;
            this.txtProjectName.UseSelectable = true;
            this.txtProjectName.WaterMark = "Enter Project Name";
            this.txtProjectName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.cbxDomainType);
            this.metroTabPage2.Controls.Add(this.metroLabel4);
            this.metroTabPage2.Controls.Add(this.cbxProjectType);
            this.metroTabPage2.Controls.Add(this.metroLabel3);
            this.metroTabPage2.Controls.Add(this.txtProjectPurpose);
            this.metroTabPage2.Controls.Add(this.txtProjectDetails);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(623, 298);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Project Details";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // cbxDomainType
            // 
            this.cbxDomainType.FormattingEnabled = true;
            this.cbxDomainType.ItemHeight = 23;
            this.cbxDomainType.Items.AddRange(new object[] {
            "Admin",
            "ProjectManager",
            "Member"});
            this.cbxDomainType.Location = new System.Drawing.Point(147, 44);
            this.cbxDomainType.Name = "cbxDomainType";
            this.cbxDomainType.Size = new System.Drawing.Size(456, 29);
            this.cbxDomainType.TabIndex = 12;
            this.cbxDomainType.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cbxDomainType.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(3, 44);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(138, 19);
            this.metroLabel4.TabIndex = 11;
            this.metroLabel4.Text = "Select Project Domain";
            // 
            // cbxProjectType
            // 
            this.cbxProjectType.FormattingEnabled = true;
            this.cbxProjectType.ItemHeight = 23;
            this.cbxProjectType.Items.AddRange(new object[] {
            "Admin",
            "ProjectManager",
            "Member"});
            this.cbxProjectType.Location = new System.Drawing.Point(147, 9);
            this.cbxProjectType.Name = "cbxProjectType";
            this.cbxProjectType.Size = new System.Drawing.Size(456, 29);
            this.cbxProjectType.TabIndex = 10;
            this.cbxProjectType.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cbxProjectType.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 9);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(119, 19);
            this.metroLabel3.TabIndex = 9;
            this.metroLabel3.Text = "Select Project Type";
            // 
            // txtProjectPurpose
            // 
            // 
            // 
            // 
            this.txtProjectPurpose.CustomButton.Image = null;
            this.txtProjectPurpose.CustomButton.Location = new System.Drawing.Point(500, 2);
            this.txtProjectPurpose.CustomButton.Name = "";
            this.txtProjectPurpose.CustomButton.Size = new System.Drawing.Size(97, 97);
            this.txtProjectPurpose.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectPurpose.CustomButton.TabIndex = 1;
            this.txtProjectPurpose.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectPurpose.CustomButton.UseSelectable = true;
            this.txtProjectPurpose.CustomButton.Visible = false;
            this.txtProjectPurpose.Lines = new string[0];
            this.txtProjectPurpose.Location = new System.Drawing.Point(3, 193);
            this.txtProjectPurpose.MaxLength = 32767;
            this.txtProjectPurpose.Multiline = true;
            this.txtProjectPurpose.Name = "txtProjectPurpose";
            this.txtProjectPurpose.PasswordChar = '\0';
            this.txtProjectPurpose.PromptText = "Enter Project Purpose";
            this.txtProjectPurpose.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectPurpose.SelectedText = "";
            this.txtProjectPurpose.SelectionLength = 0;
            this.txtProjectPurpose.SelectionStart = 0;
            this.txtProjectPurpose.ShortcutsEnabled = true;
            this.txtProjectPurpose.ShowClearButton = true;
            this.txtProjectPurpose.Size = new System.Drawing.Size(600, 102);
            this.txtProjectPurpose.TabIndex = 4;
            this.txtProjectPurpose.UseSelectable = true;
            this.txtProjectPurpose.WaterMark = "Enter Project Purpose";
            this.txtProjectPurpose.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectPurpose.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectDetails
            // 
            // 
            // 
            // 
            this.txtProjectDetails.CustomButton.Image = null;
            this.txtProjectDetails.CustomButton.Location = new System.Drawing.Point(494, 2);
            this.txtProjectDetails.CustomButton.Name = "";
            this.txtProjectDetails.CustomButton.Size = new System.Drawing.Size(103, 103);
            this.txtProjectDetails.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectDetails.CustomButton.TabIndex = 1;
            this.txtProjectDetails.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectDetails.CustomButton.UseSelectable = true;
            this.txtProjectDetails.CustomButton.Visible = false;
            this.txtProjectDetails.Lines = new string[0];
            this.txtProjectDetails.Location = new System.Drawing.Point(3, 79);
            this.txtProjectDetails.MaxLength = 32767;
            this.txtProjectDetails.Multiline = true;
            this.txtProjectDetails.Name = "txtProjectDetails";
            this.txtProjectDetails.PasswordChar = '\0';
            this.txtProjectDetails.PromptText = "Enter Project Details";
            this.txtProjectDetails.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectDetails.SelectedText = "";
            this.txtProjectDetails.SelectionLength = 0;
            this.txtProjectDetails.SelectionStart = 0;
            this.txtProjectDetails.ShortcutsEnabled = true;
            this.txtProjectDetails.ShowClearButton = true;
            this.txtProjectDetails.Size = new System.Drawing.Size(600, 108);
            this.txtProjectDetails.TabIndex = 3;
            this.txtProjectDetails.UseSelectable = true;
            this.txtProjectDetails.WaterMark = "Enter Project Details";
            this.txtProjectDetails.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectDetails.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.txtProjectScope);
            this.metroTabPage3.Controls.Add(this.txtProjectGoals);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(623, 298);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Project Goals";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // txtProjectScope
            // 
            // 
            // 
            // 
            this.txtProjectScope.CustomButton.Image = null;
            this.txtProjectScope.CustomButton.Location = new System.Drawing.Point(493, 1);
            this.txtProjectScope.CustomButton.Name = "";
            this.txtProjectScope.CustomButton.Size = new System.Drawing.Size(123, 123);
            this.txtProjectScope.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectScope.CustomButton.TabIndex = 1;
            this.txtProjectScope.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectScope.CustomButton.UseSelectable = true;
            this.txtProjectScope.CustomButton.Visible = false;
            this.txtProjectScope.Lines = new string[0];
            this.txtProjectScope.Location = new System.Drawing.Point(3, 134);
            this.txtProjectScope.MaxLength = 32767;
            this.txtProjectScope.Multiline = true;
            this.txtProjectScope.Name = "txtProjectScope";
            this.txtProjectScope.PasswordChar = '\0';
            this.txtProjectScope.PromptText = "Enter Project Scope";
            this.txtProjectScope.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectScope.SelectedText = "";
            this.txtProjectScope.SelectionLength = 0;
            this.txtProjectScope.SelectionStart = 0;
            this.txtProjectScope.ShortcutsEnabled = true;
            this.txtProjectScope.ShowClearButton = true;
            this.txtProjectScope.Size = new System.Drawing.Size(617, 125);
            this.txtProjectScope.TabIndex = 5;
            this.txtProjectScope.UseSelectable = true;
            this.txtProjectScope.WaterMark = "Enter Project Scope";
            this.txtProjectScope.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectScope.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectGoals
            // 
            // 
            // 
            // 
            this.txtProjectGoals.CustomButton.Image = null;
            this.txtProjectGoals.CustomButton.Location = new System.Drawing.Point(493, 1);
            this.txtProjectGoals.CustomButton.Name = "";
            this.txtProjectGoals.CustomButton.Size = new System.Drawing.Size(123, 123);
            this.txtProjectGoals.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectGoals.CustomButton.TabIndex = 1;
            this.txtProjectGoals.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectGoals.CustomButton.UseSelectable = true;
            this.txtProjectGoals.CustomButton.Visible = false;
            this.txtProjectGoals.Lines = new string[0];
            this.txtProjectGoals.Location = new System.Drawing.Point(3, 3);
            this.txtProjectGoals.MaxLength = 32767;
            this.txtProjectGoals.Multiline = true;
            this.txtProjectGoals.Name = "txtProjectGoals";
            this.txtProjectGoals.PasswordChar = '\0';
            this.txtProjectGoals.PromptText = "Enter Project Goals";
            this.txtProjectGoals.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectGoals.SelectedText = "";
            this.txtProjectGoals.SelectionLength = 0;
            this.txtProjectGoals.SelectionStart = 0;
            this.txtProjectGoals.ShortcutsEnabled = true;
            this.txtProjectGoals.ShowClearButton = true;
            this.txtProjectGoals.Size = new System.Drawing.Size(617, 125);
            this.txtProjectGoals.TabIndex = 4;
            this.txtProjectGoals.UseSelectable = true;
            this.txtProjectGoals.WaterMark = "Enter Project Goals";
            this.txtProjectGoals.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectGoals.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.txtProjectRisk);
            this.metroTabPage4.Controls.Add(this.txtProjectStackholder);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(623, 298);
            this.metroTabPage4.TabIndex = 3;
            this.metroTabPage4.Text = "Project Stackholder";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // txtProjectRisk
            // 
            // 
            // 
            // 
            this.txtProjectRisk.CustomButton.Image = null;
            this.txtProjectRisk.CustomButton.Location = new System.Drawing.Point(493, 1);
            this.txtProjectRisk.CustomButton.Name = "";
            this.txtProjectRisk.CustomButton.Size = new System.Drawing.Size(123, 123);
            this.txtProjectRisk.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectRisk.CustomButton.TabIndex = 1;
            this.txtProjectRisk.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectRisk.CustomButton.UseSelectable = true;
            this.txtProjectRisk.CustomButton.Visible = false;
            this.txtProjectRisk.Lines = new string[0];
            this.txtProjectRisk.Location = new System.Drawing.Point(3, 134);
            this.txtProjectRisk.MaxLength = 32767;
            this.txtProjectRisk.Multiline = true;
            this.txtProjectRisk.Name = "txtProjectRisk";
            this.txtProjectRisk.PasswordChar = '\0';
            this.txtProjectRisk.PromptText = "Enter Project Risk";
            this.txtProjectRisk.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectRisk.SelectedText = "";
            this.txtProjectRisk.SelectionLength = 0;
            this.txtProjectRisk.SelectionStart = 0;
            this.txtProjectRisk.ShortcutsEnabled = true;
            this.txtProjectRisk.ShowClearButton = true;
            this.txtProjectRisk.Size = new System.Drawing.Size(617, 125);
            this.txtProjectRisk.TabIndex = 7;
            this.txtProjectRisk.UseSelectable = true;
            this.txtProjectRisk.WaterMark = "Enter Project Risk";
            this.txtProjectRisk.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectRisk.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectStackholder
            // 
            // 
            // 
            // 
            this.txtProjectStackholder.CustomButton.Image = null;
            this.txtProjectStackholder.CustomButton.Location = new System.Drawing.Point(493, 1);
            this.txtProjectStackholder.CustomButton.Name = "";
            this.txtProjectStackholder.CustomButton.Size = new System.Drawing.Size(123, 123);
            this.txtProjectStackholder.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectStackholder.CustomButton.TabIndex = 1;
            this.txtProjectStackholder.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectStackholder.CustomButton.UseSelectable = true;
            this.txtProjectStackholder.CustomButton.Visible = false;
            this.txtProjectStackholder.Lines = new string[0];
            this.txtProjectStackholder.Location = new System.Drawing.Point(3, 3);
            this.txtProjectStackholder.MaxLength = 32767;
            this.txtProjectStackholder.Multiline = true;
            this.txtProjectStackholder.Name = "txtProjectStackholder";
            this.txtProjectStackholder.PasswordChar = '\0';
            this.txtProjectStackholder.PromptText = "Enter Project Stackholders";
            this.txtProjectStackholder.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectStackholder.SelectedText = "";
            this.txtProjectStackholder.SelectionLength = 0;
            this.txtProjectStackholder.SelectionStart = 0;
            this.txtProjectStackholder.ShortcutsEnabled = true;
            this.txtProjectStackholder.ShowClearButton = true;
            this.txtProjectStackholder.Size = new System.Drawing.Size(617, 125);
            this.txtProjectStackholder.TabIndex = 6;
            this.txtProjectStackholder.UseSelectable = true;
            this.txtProjectStackholder.WaterMark = "Enter Project Stackholders";
            this.txtProjectStackholder.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectStackholder.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.txtProjectAssumptions);
            this.metroTabPage5.Controls.Add(this.txtProjectBudgetRequirement);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(623, 298);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Project Assumptions";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // txtProjectAssumptions
            // 
            // 
            // 
            // 
            this.txtProjectAssumptions.CustomButton.Image = null;
            this.txtProjectAssumptions.CustomButton.Location = new System.Drawing.Point(493, 1);
            this.txtProjectAssumptions.CustomButton.Name = "";
            this.txtProjectAssumptions.CustomButton.Size = new System.Drawing.Size(123, 123);
            this.txtProjectAssumptions.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectAssumptions.CustomButton.TabIndex = 1;
            this.txtProjectAssumptions.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectAssumptions.CustomButton.UseSelectable = true;
            this.txtProjectAssumptions.CustomButton.Visible = false;
            this.txtProjectAssumptions.Lines = new string[0];
            this.txtProjectAssumptions.Location = new System.Drawing.Point(0, 134);
            this.txtProjectAssumptions.MaxLength = 32767;
            this.txtProjectAssumptions.Multiline = true;
            this.txtProjectAssumptions.Name = "txtProjectAssumptions";
            this.txtProjectAssumptions.PasswordChar = '\0';
            this.txtProjectAssumptions.PromptText = "Enter Project Assumptions";
            this.txtProjectAssumptions.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectAssumptions.SelectedText = "";
            this.txtProjectAssumptions.SelectionLength = 0;
            this.txtProjectAssumptions.SelectionStart = 0;
            this.txtProjectAssumptions.ShortcutsEnabled = true;
            this.txtProjectAssumptions.ShowClearButton = true;
            this.txtProjectAssumptions.Size = new System.Drawing.Size(617, 125);
            this.txtProjectAssumptions.TabIndex = 9;
            this.txtProjectAssumptions.UseSelectable = true;
            this.txtProjectAssumptions.WaterMark = "Enter Project Assumptions";
            this.txtProjectAssumptions.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectAssumptions.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtProjectBudgetRequirement
            // 
            // 
            // 
            // 
            this.txtProjectBudgetRequirement.CustomButton.Image = null;
            this.txtProjectBudgetRequirement.CustomButton.Location = new System.Drawing.Point(493, 1);
            this.txtProjectBudgetRequirement.CustomButton.Name = "";
            this.txtProjectBudgetRequirement.CustomButton.Size = new System.Drawing.Size(123, 123);
            this.txtProjectBudgetRequirement.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProjectBudgetRequirement.CustomButton.TabIndex = 1;
            this.txtProjectBudgetRequirement.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProjectBudgetRequirement.CustomButton.UseSelectable = true;
            this.txtProjectBudgetRequirement.CustomButton.Visible = false;
            this.txtProjectBudgetRequirement.Lines = new string[0];
            this.txtProjectBudgetRequirement.Location = new System.Drawing.Point(0, 3);
            this.txtProjectBudgetRequirement.MaxLength = 32767;
            this.txtProjectBudgetRequirement.Multiline = true;
            this.txtProjectBudgetRequirement.Name = "txtProjectBudgetRequirement";
            this.txtProjectBudgetRequirement.PasswordChar = '\0';
            this.txtProjectBudgetRequirement.PromptText = "Enter Project Budget Requirment";
            this.txtProjectBudgetRequirement.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtProjectBudgetRequirement.SelectedText = "";
            this.txtProjectBudgetRequirement.SelectionLength = 0;
            this.txtProjectBudgetRequirement.SelectionStart = 0;
            this.txtProjectBudgetRequirement.ShortcutsEnabled = true;
            this.txtProjectBudgetRequirement.ShowClearButton = true;
            this.txtProjectBudgetRequirement.Size = new System.Drawing.Size(617, 125);
            this.txtProjectBudgetRequirement.TabIndex = 8;
            this.txtProjectBudgetRequirement.UseSelectable = true;
            this.txtProjectBudgetRequirement.WaterMark = "Enter Project Budget Requirment";
            this.txtProjectBudgetRequirement.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProjectBudgetRequirement.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(518, 409);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(136, 33);
            this.btnNext.TabIndex = 12;
            this.btnNext.Text = "Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Location = new System.Drawing.Point(12, 409);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(136, 33);
            this.btnPrev.TabIndex = 13;
            this.btnPrev.Text = "Previous";
            this.btnPrev.UseSelectable = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(261, 409);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(136, 33);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(504, 24);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(136, 33);
            this.btnImport.TabIndex = 15;
            this.btnImport.Text = "Import";
            this.btnImport.UseSelectable = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // ManageProjectCharterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 454);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.tabControl);
            this.Name = "ManageProjectCharterForm";
            this.Text = "Manage ProjectCharterForm";
            this.Load += new System.EventHandler(this.ManageProjectCharterForm_Load);
            this.tabControl.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTabControl tabControl;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroDateTime dpEndDate;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroDateTime dpStartDate;
        private MetroFramework.Controls.MetroTextBox txtProjectSponser;
        private MetroFramework.Controls.MetroTextBox txtProjectManagerName;
        private MetroFramework.Controls.MetroTextBox txtProjectRequester;
        private MetroFramework.Controls.MetroTextBox txtProjectAuthor;
        private MetroFramework.Controls.MetroTextBox txtProjectName;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroButton btnPrev;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroTextBox txtProjectDetails;
        private MetroFramework.Controls.MetroTextBox txtProjectPurpose;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTextBox txtProjectGoals;
        private MetroFramework.Controls.MetroTextBox txtProjectScope;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroTextBox txtProjectRisk;
        private MetroFramework.Controls.MetroTextBox txtProjectStackholder;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroTextBox txtProjectAssumptions;
        private MetroFramework.Controls.MetroTextBox txtProjectBudgetRequirement;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox cbxProjectType;
        private MetroFramework.Controls.MetroComboBox cbxDomainType;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton btnImport;
    }
}