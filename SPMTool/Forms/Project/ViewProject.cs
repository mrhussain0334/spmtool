﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using SPMTool.Models;
using SPMTool.UserControls;

namespace SPMTool.Forms.Project
{
    public partial class ViewProject : MetroForm
    {
        private ProjectCharter _charter;
        public ViewProject(ProjectCharter charter)
        {
            _charter = charter;
            InitializeComponent();
        }

        private void ViewProject_Load(object sender, EventArgs e)
        {
            SetTileLocation(btnOverview);
            Clear();
            parentPanel.Controls.Add(new OverviewProject(_charter));
        }

        #region OtherMethods

        private void SetTileLocation(Control btn)
        {
            metroTile.Top = btn.Top;
            metroTile.Height = btn.Height;
        }
        void Clear()
        {
            parentPanel.Controls.Clear();
        }


        #endregion

        private void btnOverview_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnOverview);
            Clear();
            parentPanel.Controls.Add(new OverviewProject(_charter));

        }

        private void btnRequirements_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnRequirements);
            Clear();
            parentPanel.Controls.Add(new AllRequirements(_charter));

        }

        private void btnWbs_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnWbs);
            Clear();
            parentPanel.Controls.Add(new AllWBS(_charter));
        }

        private void btnAssignUser_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnAssignUser);
            Clear();
            parentPanel.Controls.Add(new AssignUser(_charter.ProjectId));
        }
    }
}
