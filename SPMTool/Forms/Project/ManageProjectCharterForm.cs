﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;
using OfficeOpenXml;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms.Project
{
    public partial class ManageProjectCharterForm : MetroForm
    {
        private bool isEdit;
        private int projectCharterId;
        private readonly ProjectRepository repository = new ProjectRepository();
        private readonly ProjectCharterRepository _projectCharterRepository = new ProjectCharterRepository();
        private readonly ProjectDomainRepository _projectDomainRepository = new ProjectDomainRepository();
        private readonly ProjectTypeRepository _projectTypeRepository = new ProjectTypeRepository();
        private Models.ProjectCharter _project;
        public ManageProjectCharterForm(bool isEdit, int projectCharterId = 0)
        {
            this.isEdit = isEdit;
            this.projectCharterId = projectCharterId;
            InitializeComponent();
        }

        private void ManageProjectCharterForm_Load(object sender, EventArgs e)
        {

            cbxProjectType.DataSource = _projectTypeRepository.GetAllTypes();
            cbxProjectType.ValueMember = "Id";
            cbxProjectType.DisplayMember = "ProjectTypeName";

            cbxDomainType.DataSource = _projectDomainRepository.GetAllDomains();
            cbxDomainType.ValueMember = "Id";
            cbxDomainType.DisplayMember = "ProjectDomainName";

            txtProjectManagerName.Text = ConfigService.Instance.Name;
            txtProjectManagerName.ReadOnly = true;

            if (isEdit)
            {
                Text = @"Edit Project Charter";
                if (projectCharterId > 0)
                {
                    _project = _projectCharterRepository.GetProjectCharterByProjectId(projectCharterId);
                    SetValues();
                }
            }
            else
            {
                Text = @"Create New Project Charter";
            }
        }

        private void SetValues()
        {
            var charter = _project;
            txtProjectName.Text = _project.Project.Name;
            txtProjectAssumptions.Text = charter.Assumptions;
            txtProjectAuthor.Text = charter.Author;
            txtProjectBudgetRequirement.Text = charter.BudgetRequirements;
            txtProjectDetails.Text = charter.Description;
            txtProjectGoals.Text = charter.Goals;
            txtProjectManagerName.Text = _project.Project.ProjectManager.FirstName + " " + _project.Project.ProjectManager.LastName;
            txtProjectManagerName.ReadOnly = true;
            txtProjectPurpose.Text = charter.Purpose;
            txtProjectRequester.Text = charter.Requester;
            txtProjectRisk.Text = charter.Risks;
            cbxProjectType.SelectedIndex = cbxProjectType.FindStringExact(_project.Project.ProjectType.ProjectTypeName);
            cbxDomainType.SelectedIndex = cbxDomainType.FindStringExact(_project.Project.ProjectDomain.ProjectDomainName);
            txtProjectScope.Text = charter.Scope;
            txtProjectStackholder.Text = charter.StakeHolders;
            txtProjectSponser.Text = charter.Sponser;
            dpStartDate.Value = charter.StartDate;
            dpEndDate.Value = charter.EndDate;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            var i = tabControl.SelectedIndex;
            if (i == tabControl.TabCount - 1)
            {
                if (!isEdit)
                {
                    if (ValidateValues())
                    {
                        MessagBoxService.Show(this, @"You have created a Project", "Success");
                        Reset();
                    }
                }
                else
                {
                    if (VerifyUpdate())
                    {
                        MessagBoxService.Show(this, @"You have Updated Project Details", "Success");
                    }
                }
            }
            else
            {
                tabControl.SelectedIndex = i + 1;

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Reset()
        {
            foreach (Control control in Controls)
            {
                if (control.GetType() == typeof(MetroTextBox))
                    control.Text = "";
            }
            cbxProjectType.SelectedIndex = cbxDomainType.SelectedIndex = 0;
        }

        private bool VerifyUpdate()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            var project = new Models.Project()
            {
                Description = txtProjectDetails.Text,
                Name = txtProjectName.Text,
                ProjectDomainId = int.Parse(cbxDomainType.SelectedValue.ToString()),
                ProjectTypeId = int.Parse(cbxProjectType.SelectedValue.ToString()),
                UpdateAt = DateTime.Now,
            };

            repository.Update(project, _project.ProjectId);
            var charter = new ProjectCharter()
            {
                Assumptions = txtProjectAssumptions.Text,
                Author = txtProjectAuthor.Text,
                BudgetRequirements = txtProjectBudgetRequirement.Text,
                CreatedAt = DateTime.Now,
                Description = txtProjectDetails.Text,
                EndDate = dpEndDate.Value,
                Goals = txtProjectGoals.Text,
                ProjectId = project.Id,
                ProjectName = txtProjectName.Text,
                Purpose = txtProjectPurpose.Text,
                Requester = txtProjectRequester.Text,
                Risks = txtProjectRisk.Text,
                Scope = txtProjectScope.Text,
                Sponser = txtProjectSponser.Text,
                StakeHolders = txtProjectStackholder.Text,
                StartDate = dpStartDate.Value,
                UpdateAt = DateTime.Now,
            };

            _projectCharterRepository.Update(charter,projectCharterId);
            return true;
        }
        private bool ValidateValues()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            var project = new Models.Project()
            {
                CreatedAt = DateTime.Now,
                Description = txtProjectDetails.Text,
                Name = txtProjectName.Text,
                ProjectDomainId = int.Parse(cbxDomainType.SelectedValue.ToString()),
                ProjectManagerId = ConfigService.Instance.Id,
                ProjectTypeId = int.Parse(cbxProjectType.SelectedValue.ToString()),
                UpdateAt = DateTime.Now,
            };

            repository.Add(project);
            var charter = new ProjectCharter()
            {
                Assumptions = txtProjectAssumptions.Text,
                Author = txtProjectAuthor.Text,
                BudgetRequirements = txtProjectBudgetRequirement.Text,
                CreatedAt = DateTime.Now,
                Description = txtProjectDetails.Text,
                EndDate = dpEndDate.Value,
                Goals = txtProjectGoals.Text,
                ProjectId = project.Id,
                ProjectName = txtProjectName.Text,
                Purpose = txtProjectPurpose.Text,
                Requester = txtProjectRequester.Text,
                Risks = txtProjectRisk.Text,
                Scope = txtProjectScope.Text,
                Sponser = txtProjectSponser.Text,
                StakeHolders = txtProjectStackholder.Text,
                StartDate = dpStartDate.Value,
                UpdateAt = DateTime.Now,
            };

            _projectCharterRepository.Add(charter);
            return true;
        }
        private void btnPrev_Click(object sender, EventArgs e)
        {
            var i = tabControl.SelectedIndex;
            if (i > 0)
                tabControl.SelectedIndex = i - 1;
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == tabControl.TabCount - 1)
            {
                btnNext.Text = !isEdit ? @"Save Project Charter" : @"Update Project Charter";
            }
            else
            {
                btnNext.Text = @"Next";
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog { Filter = @"Excel Files|*.xls;*.xlsx;*.xlsm", Multiselect = false };
            var result = fileDialog.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                var newFile = new FileInfo(fileDialog.FileName);
                using (var pck = new ExcelPackage(newFile))
                {
                    //Add the Content sheet
                    var ws = pck.Workbook.Worksheets["Sheet1"];
                    txtProjectDetails.Text = ws.Cells["B19"].Text;
                    txtProjectAssumptions.Text = ws.Cells["B110"].Text;
                    txtProjectAuthor.Text = ws.Cells["B8"].Text;
                    txtProjectBudgetRequirement.Text = ws.Cells["B113"].Text;
                    txtProjectGoals.Text = ws.Cells["B38"].Text;
                    txtProjectName.Text = ws.Cells["B3"].Text;
                    txtProjectPurpose.Text = ws.Cells["B32"].Text;
                    txtProjectRequester.Text = ws.Cells["B10"].Text;
                    txtProjectRisk.Text = ws.Cells["B97"].Text;
                    txtProjectScope.Text = ws.Cells["B54"].Text;
                    txtProjectSponser.Text = ws.Cells["B13"].Text;
                    txtProjectStackholder.Text = ws.Cells["B92"].Text;
                    dpStartDate.Value = DateTime.Parse(ws.Cells["B16"].Value.ToString());
                    dpEndDate.Value = DateTime.Parse(ws.Cells["B17"].Value.ToString());
                }
            }
            Console.WriteLine(result);
        }
    }
}
