﻿namespace SPMTool.Forms.WBS
{
    partial class WbsDiagramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelWbs = new MetroFramework.Controls.MetroPanel();
            this.btnGenerate = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // panelWbs
            // 
            this.panelWbs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelWbs.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelWbs.HorizontalScrollbarBarColor = true;
            this.panelWbs.HorizontalScrollbarHighlightOnWheel = false;
            this.panelWbs.HorizontalScrollbarSize = 10;
            this.panelWbs.Location = new System.Drawing.Point(24, 139);
            this.panelWbs.Name = "panelWbs";
            this.panelWbs.Size = new System.Drawing.Size(591, 273);
            this.panelWbs.TabIndex = 0;
            this.panelWbs.VerticalScrollbarBarColor = true;
            this.panelWbs.VerticalScrollbarHighlightOnWheel = false;
            this.panelWbs.VerticalScrollbarSize = 10;
            this.panelWbs.Paint += new System.Windows.Forms.PaintEventHandler(this.panelWbs_Paint);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(24, 100);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(136, 33);
            this.btnGenerate.TabIndex = 13;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseSelectable = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // WbsDiagramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 435);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.panelWbs);
            this.Name = "WbsDiagramForm";
            this.Text = "WbsDiagramForm";
            this.Load += new System.EventHandler(this.WbsDiagramForm_Load);
            this.Scroll += new System.Windows.Forms.ScrollEventHandler(this.WbsDiagramForm_Scroll);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel panelWbs;
        private MetroFramework.Controls.MetroButton btnGenerate;
    }
}