﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Forms;
using SPMTool.Models;
using SPMTool.Repository;

namespace SPMTool.Forms.WBS
{
    public partial class WbsDiagramForm : MetroForm
    {
        private readonly ProjectCharter _charter;
        private readonly WbsRepository _wbsRepository = new WbsRepository();
        private IEnumerable<Wbs> _list;
        public WbsDiagramForm(ProjectCharter charter)
        {
            _charter = charter;
            InitializeComponent();
        }

        private void WbsDiagramForm_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            Text = @"WBS Diagram : " + _charter.ProjectName;
            _list = _wbsRepository.GetWbsesByProjectId(_charter.ProjectId);
            panelWbs.Height = 1000;
            AutoScroll = false;
            AutoScroll = true;
        }

        private void panelWbs_Paint(object sender, PaintEventArgs e)
        {
        }

        private const float BoxWidth = 160;
        private const float BoxHalfWidth = BoxWidth / 2;
        private const float BoxHeight = 60;
        private const float BoxHalfHeight = BoxHeight/2;
        private void DrawSingleBox(Wbs wbs, Graphics g, float x, float y)
        {
            var p = new Pen(Color.Blue);
            Brush b = new SolidBrush(Color.Blue);
            Brush b2 = new SolidBrush(Color.White);
            g.FillRectangle(b, x, y, BoxWidth, BoxHalfHeight);
            g.DrawRectangle(p, x, y, BoxWidth, BoxHalfHeight);
            g.DrawString(wbs.Code, new Font(FontFamily.GenericSerif, 8, FontStyle.Bold), b2, x + BoxHalfWidth, y + 15);
            g.DrawRectangle(p, x, y + BoxHalfHeight, BoxWidth, BoxHalfHeight);
            g.DrawString(wbs.Package, new Font(FontFamily.GenericSerif, 8, FontStyle.Bold), b, x + 10, y + 40);
        }

        private readonly SingleBox _headBox = new SingleBox();
        private readonly List<SingleBox> _secondlist = new List<SingleBox>();

        private void btnGenerate_Click(object sender, EventArgs e)
        {
         
            _secondlist.Clear();
            Graphics g = panelWbs.CreateGraphics();
            var levelOne = _list.Single(p => p.Code.Split('.').Length == 1);
            var levelTwo = _list.Where(p => p.Code.Split('.').Length == 2).ToList();
            var mid = panelWbs.Width / 2;
            DrawSingleBox(levelOne, g, mid, 10);
            _headBox.X = mid;
            _headBox.Y = 10;
            _headBox.Wbs = levelOne;
            float start = panelWbs.Width / levelTwo.Count;
            foreach (var wbse in levelTwo)
            {
                DrawSingleBox(wbse,g,start,BoxHeight + 50);

                var level2 = new SingleBox()
                {
                    Wbs = wbse,
                    X = start,
                    Y = BoxHeight + 50
                };
                _secondlist.Add(level2);
                DrawLine(_headBox, level2, true);

                var level3s = _list.Where(p => p.Code.Split('.').Length == 3 && p.Code.StartsWith(wbse.Code + ".")).ToList();
                var y = level2.Y + BoxHeight + 10;
                foreach (var level3 in level3s)
                {
                    DrawSingleBox(level3,g,start + 30,y);
                    var l3 = new SingleBox()
                    {
                        Head = level2,
                        Wbs = level3,
                        X = start + 30,
                        Y = y,
                    };
                    _secondlist.Add(l3);
                    y += BoxHeight + 10;
                    DrawLine(level2, l3, false);

                }
                start += (BoxWidth + 50f);
            }

        }

        private void WbsDiagramForm_Scroll(object sender, ScrollEventArgs e)
        {
            var g = panelWbs.CreateGraphics();
            DrawSingleBox(_headBox.Wbs,g,_headBox.X,_headBox.Y);
            foreach (var wbse in _secondlist)
            {
                DrawSingleBox(wbse.Wbs,g,wbse.X,wbse.Y);
                if (wbse.Head != null)
                {
                    DrawLine(wbse.Head,wbse);   
                }
                else
                {
                    DrawLine(_headBox,wbse,true);
                }
            }
        }

        private void DrawLine(SingleBox head,SingleBox current,bool isDirect = false)
        {
            if (!isDirect)
            {
                var g = panelWbs.CreateGraphics();
                var pen = new Pen(Color.Blue);
                var arrowPen = new Pen(Color.Blue) {CustomEndCap = new AdjustableArrowCap(5, 5, true)};
                g.DrawLine(arrowPen, head.X, current.Y + (int) BoxHalfHeight, current.X, current.Y + (int) BoxHalfHeight);
                g.DrawLine(pen, head.X, head.Y + BoxHeight, head.X, current.Y + (int) BoxHalfHeight);
            }
            else
            {
                var g = panelWbs.CreateGraphics();
                var arrowPen = new Pen(Color.Blue) { CustomEndCap = new AdjustableArrowCap(5, 5, true) };
                g.DrawLine(arrowPen, head.X + BoxHalfWidth, head.Y + BoxHeight , current.X + BoxHalfWidth, current.Y);
            }
        }
    }

    public class SingleBox
    {
        public SingleBox Head { get; set; }
        public Wbs Wbs { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
    }

}
