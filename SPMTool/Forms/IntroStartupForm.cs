﻿using System;
using System.ComponentModel;
using System.Threading;
using MetroFramework.Forms;

namespace SPMTool.Forms
{
    public partial class IntroStartupForm : MetroForm
    {
        private const double Seconds = 5;
        public IntroStartupForm()
        {
            InitializeComponent();
        }

        private void IntroStartupForm_Load(object sender, EventArgs e)
        {
            backgroundWorker.RunWorkerAsync();
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Close();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progBar.Value = e.ProgressPercentage;
            lblProgress.Text = @"Loading..." + progBar.Value;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (var i = 1; i <= 100; i++)
            {
                Thread.Sleep(50);
                backgroundWorker.ReportProgress(i);
            }
        }
    }
}
