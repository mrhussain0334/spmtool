﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using SPMTool.Services;
using SPMTool.UserControls;

namespace SPMTool.Forms
{
    public partial class ProjectManagerDashboard : MetroForm
    {
        public ProjectManagerDashboard()
        {
            InitializeComponent();
        }

        private void ProjectManagerDashboard_Load(object sender, EventArgs e)
        {
            SetTileLocation(btnUsers);
            Clear();
            parentPanel.Controls.Add(new AllUsers(false));
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnUsers);
            Clear();
            parentPanel.Controls.Add(new AllUsers(false));
        }

        #region OtherMethods

        private void SetTileLocation(Control btn)
        {
            metroTile.Top = btn.Top;
            metroTile.Height = btn.Height;
        }
        void Clear()
        {
            parentPanel.Controls.Clear();
        }


        #endregion

        private void btnAllProjects_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnAllProjects);
            Clear();
            parentPanel.Controls.Add(new AllProjects(false));
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Abort;
                Close();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
        }

        private void btnFinishProject_Click(object sender, EventArgs e)
        {

        }
    }
}
