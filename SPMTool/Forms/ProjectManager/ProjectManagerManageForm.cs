﻿using System;
using System.Linq;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms
{
    public partial class ProjectManagerManageForm : MetroForm
    {
        private readonly bool _isEdit;
        private readonly int _projectManagerId;
        private ProjectManager _projectManager;
        public ProjectManagerManageForm(bool isEdit, int projectManagerId = 0)
        {
            InitializeComponent();
            _isEdit = isEdit;
            _projectManagerId = projectManagerId;
        }
        private void ProjectManagerManageForm_Load(object sender, EventArgs e)
        {
            rbMale.Checked = true;
            if (_isEdit)
            {
                Text = @"Edit Project Manager";
                Controls.Remove(txtPassword);
                Controls.Remove(txtUsername);
                Controls.Remove(txtVerifyPassword);
                if (_projectManagerId > 0)
                {
                    _projectManager = new ProjectManagerRepository().GetById(_projectManagerId);
                    SetValues();
                    btnCreate.Text = @"Update Manager";
                }
                else
                {
                    MessagBoxService.Show(this,"Error Occured","Error");
                    Close();
                }
            }
            else
            {
                Text = @"Create Project Manager";
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                if (VerifyAdd())
                {
                    MessagBoxService.Show(this, "Manager Added", "");
                    Reset();
                }
            }
            else
            {
                if (VerifyUpdate())
                {
                    MessagBoxService.Show(this, "Manager Updated", "");
                    Close();
                }
            }
        }


        private void Reset()
        {
            var list = Controls;
            foreach (Control control in list)
            {
                if (control.GetType() == typeof(MetroTextBox))
                {
                    control.Text = "";
                }
            }
        }
        private bool VerifyAdd()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox) control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this,"Fill All the Boxes","Error");
                return false;
            }
            var repo = new ProjectManagerRepository();
            if (!repo.VerifyIfUsernameAvailable(txtUsername.Text))
            {
                MetroMessageBox.Show(this, "Username Already Exists", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Asterisk);
                return false;
            }
            if (txtPassword.Text != txtVerifyPassword.Text)
            {
                return false;
            }
            var manager = new ProjectManager()
            {
                Address = txtAddress.Text,
                CreatedAt = DateTime.Now,
                CreatedByAdminId = ConfigService.Instance.Id,
                Email = txtEmail.Text,
                FirstName = txtFName.Text,
                Gender = rbMale.Checked ? "Male" : "Female",
                LastName = txtLName.Text,
                Username = txtUsername.Text,
                ContactNumber = txtContactNumber.Text,
                Password = PasswordService.EncryptPassword(txtPassword.Text),
                Qualifications = txtQualifications.Text,
                UpdateAt = DateTime.Now
            };

            repo.Add(manager);
            return true;
        }
        private bool VerifyUpdate()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            var repo = new ProjectManagerRepository();
            if (!repo.VerifyIfUsernameAvailable(txtUsername.Text))
            {
                MetroMessageBox.Show(this, "Username Already Exists", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Asterisk);
                return false;
            }
            var manager = new ProjectManager()
            {
                Address = txtAddress.Text,
                CreatedAt = DateTime.Now,
                CreatedByAdminId = ConfigService.Instance.Id,
                Email = txtEmail.Text,
                FirstName = txtFName.Text,
                Gender = rbMale.Checked ? "Male" : "Female",
                LastName = txtLName.Text,
                Username = txtUsername.Text,
                ContactNumber = txtContactNumber.Text,
                Password = PasswordService.EncryptPassword(txtPassword.Text),
                Qualifications = txtQualifications.Text,
                UpdateAt = DateTime.Now
            };
            repo.Update(manager,_projectManagerId);
            return true;
        }

        private void SetValues()
        {
            txtFName.Text = _projectManager.FirstName;
            txtLName.Text = _projectManager.LastName;
            txtAddress.Text = _projectManager.Address;
            txtContactNumber.Text = _projectManager.ContactNumber;
            txtEmail.Text = _projectManager.Email;
            txtQualifications.Text = _projectManager.Qualifications;
            if(_projectManager.Gender == "Male")
            {
                rbMale.Checked = true;
                rbFemale.Checked = false;
            }
            else
            {
                rbMale.Checked = false;
                rbFemale.Checked = true;
            }
        }



        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }
    }
}
