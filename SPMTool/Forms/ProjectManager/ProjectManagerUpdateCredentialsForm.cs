﻿using System;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;
using SPMTool.Models;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms
{
    public partial class ProjectManagerUpdateCredentialsForm : MetroForm
    {
        private int _projectManagerId;
        private ProjectManager _projectManager;
        readonly ProjectManagerRepository _repo = new ProjectManagerRepository();

        public ProjectManagerUpdateCredentialsForm(int projectManagerId)
        {
            InitializeComponent();
            _projectManagerId = projectManagerId;
        }

        private void ProjectManagerUpdateCredentialsForm_Load(object sender, EventArgs e)
        {
            if (_projectManagerId > 0)
            {
                _projectManager = _repo.GetById(_projectManagerId);
                txtUsername.Text = _projectManager.Username;
            }
        }

        private bool Verify()
        {
            if ((from Control control in Controls where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            if (txtPassword.Text != txtVerifyPassword.Text)
            {
                return false;
            }
            return true;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Verify())
                {
                    var obj = new ProjectManager()
                    {
                        Username = txtUsername.Text,
                        Password = PasswordService.EncryptPassword(txtPassword.Text)
                    };
                    _repo.UpdateCredentials(obj, _projectManagerId);
                    MessagBoxService.Show(this, "Credentials Updated", "Success");
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception.Message,"Error");
            }
        }
    }
}
