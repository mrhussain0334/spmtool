﻿using System;
using MetroFramework.Forms;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms.User
{
    public partial class CompleteRequirementForm : MetroForm
    {
        private int _requirementId;
        private readonly RequirementRepository _requirementRepository = new RequirementRepository();
        public CompleteRequirementForm(int requirementId)
        {
            _requirementId = requirementId;
            InitializeComponent();
        }

        private void CompleteRequirementForm_Load(object sender, EventArgs e)
        {
            try
            {
                var r = _requirementRepository.GetRequirementById(_requirementId);
                txtDescription.Text = r.Description;
                txtProject.Text = r.Project.ToString();
                txtRequirementCode.Text = r.Code;
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
        }

        private void btnComplete_Click(object sender, EventArgs e)
        {
            try
            {
                var r = _requirementRepository.GetRequirementById(_requirementId);
                r.ActualStartDate = dtStart.Value;
                r.ActualEndDate = dtEnd.Value;
                r.IsCompleted = true;
                _requirementRepository.Update(r);
                MessagBoxService.Show(this, "Requirement Completed","Success");
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
        }
    }
}
