﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using SPMTool.Services;
using SPMTool.UserControls;

namespace SPMTool.Forms.User
{
    public partial class UserDashboard : MetroForm
    {
        public UserDashboard()
        {
            InitializeComponent();
        }

        private void UserDashboard_Load(object sender, EventArgs e)
        {
            SetTileLocation(btnUsers);
            Clear();
            parentPanel.Controls.Add(new UserRequirements());
        }

        #region OtherMethods

        private void SetTileLocation(Control btn)
        {
            metroTile.Top = btn.Top;
            metroTile.Height = btn.Height;
        }
        void Clear()
        {
            parentPanel.Controls.Clear();
        }


        #endregion

        private void btnUsers_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnUsers);
            Clear();
            parentPanel.Controls.Add(new UserRequirements());
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Abort;
                Close();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception);
            }
        }
    }
}
