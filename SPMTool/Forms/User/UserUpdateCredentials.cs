﻿using System;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms.User
{
    public partial class UserUpdateCredentials : MetroForm
    {
        private int userId;
        readonly UserRepository _repo = new UserRepository();


        public UserUpdateCredentials(int userId)
        {
            InitializeComponent();
            this.userId = userId;
        }

        private bool Verify()
        {
            if ((from Control control in Controls where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            if (txtPassword.Text != txtVerifyPassword.Text)
            {
                return false;
            }
            return true;
        }
        private void UserUpdateCredentials_Load(object sender, EventArgs e)
        {
            if (userId > 0)
            {
                var user = _repo.GetUserById(userId);
                txtUsername.Text = user.Username;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Verify())
                {
                    var obj = new Models.User()
                    {
                        Username = txtUsername.Text,
                        Password = PasswordService.EncryptPassword(txtPassword.Text)
                    };
                    _repo.UpdateCredentials(obj, userId);
                    MessagBoxService.Show(this, "Credentials Updated", "Success");
                }
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this, exception.Message, "Error");
            }
        }
    }
}
