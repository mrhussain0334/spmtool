﻿namespace SPMTool.Forms.User
{
    partial class CompleteRequirementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtRequirementCode = new MetroFramework.Controls.MetroLabel();
            this.txtDescription = new MetroFramework.Controls.MetroLabel();
            this.txtProject = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.dtStart = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.dtEnd = new MetroFramework.Controls.MetroDateTime();
            this.btnComplete = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(24, 87);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(41, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Code";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(24, 126);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(74, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Description";
            // 
            // txtRequirementCode
            // 
            this.txtRequirementCode.AutoSize = true;
            this.txtRequirementCode.Location = new System.Drawing.Point(155, 87);
            this.txtRequirementCode.Name = "txtRequirementCode";
            this.txtRequirementCode.Size = new System.Drawing.Size(41, 19);
            this.txtRequirementCode.TabIndex = 2;
            this.txtRequirementCode.Text = "Code";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = true;
            this.txtDescription.Location = new System.Drawing.Point(155, 126);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(74, 19);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.Text = "Description";
            // 
            // txtProject
            // 
            this.txtProject.AutoSize = true;
            this.txtProject.Location = new System.Drawing.Point(155, 174);
            this.txtProject.Name = "txtProject";
            this.txtProject.Size = new System.Drawing.Size(64, 19);
            this.txtProject.TabIndex = 5;
            this.txtProject.Text = "txtProject";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 174);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(50, 19);
            this.metroLabel4.TabIndex = 4;
            this.metroLabel4.Text = "Project";
            // 
            // dtStart
            // 
            this.dtStart.Location = new System.Drawing.Point(155, 226);
            this.dtStart.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtStart.Name = "dtStart";
            this.dtStart.Size = new System.Drawing.Size(245, 29);
            this.dtStart.TabIndex = 6;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(24, 226);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(77, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "Actual Start";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(24, 290);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(102, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "Actual End Date";
            // 
            // dtEnd
            // 
            this.dtEnd.Location = new System.Drawing.Point(155, 290);
            this.dtEnd.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(245, 29);
            this.dtEnd.TabIndex = 8;
            // 
            // btnComplete
            // 
            this.btnComplete.Location = new System.Drawing.Point(24, 355);
            this.btnComplete.Name = "btnComplete";
            this.btnComplete.Size = new System.Drawing.Size(136, 38);
            this.btnComplete.TabIndex = 10;
            this.btnComplete.Text = "Complete";
            this.btnComplete.UseSelectable = true;
            this.btnComplete.Click += new System.EventHandler(this.btnComplete_Click);
            // 
            // CompleteRequirementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 503);
            this.Controls.Add(this.btnComplete);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.dtEnd);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.dtStart);
            this.Controls.Add(this.txtProject);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtRequirementCode);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "CompleteRequirementForm";
            this.Text = "CompleteRequirementForm";
            this.Load += new System.EventHandler(this.CompleteRequirementForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel txtRequirementCode;
        private MetroFramework.Controls.MetroLabel txtDescription;
        private MetroFramework.Controls.MetroLabel txtProject;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroDateTime dtStart;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroDateTime dtEnd;
        private MetroFramework.Controls.MetroButton btnComplete;
    }
}