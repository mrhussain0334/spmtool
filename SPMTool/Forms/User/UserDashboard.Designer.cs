﻿namespace SPMTool.Forms.User
{
    partial class UserDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDashboard));
            this.parentPanel = new MetroFramework.Controls.MetroPanel();
            this.optionPanel = new MetroFramework.Controls.MetroPanel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.metroTile = new System.Windows.Forms.Panel();
            this.btnUsers = new System.Windows.Forms.Button();
            this.optionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // parentPanel
            // 
            this.parentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parentPanel.HorizontalScrollbarBarColor = true;
            this.parentPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.parentPanel.HorizontalScrollbarSize = 10;
            this.parentPanel.Location = new System.Drawing.Point(194, 60);
            this.parentPanel.Name = "parentPanel";
            this.parentPanel.Size = new System.Drawing.Size(638, 466);
            this.parentPanel.TabIndex = 5;
            this.parentPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.parentPanel.VerticalScrollbarBarColor = true;
            this.parentPanel.VerticalScrollbarHighlightOnWheel = false;
            this.parentPanel.VerticalScrollbarSize = 10;
            // 
            // optionPanel
            // 
            this.optionPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.optionPanel.Controls.Add(this.btnLogout);
            this.optionPanel.Controls.Add(this.metroTile);
            this.optionPanel.Controls.Add(this.btnUsers);
            this.optionPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.optionPanel.HorizontalScrollbarBarColor = true;
            this.optionPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.optionPanel.HorizontalScrollbarSize = 10;
            this.optionPanel.Location = new System.Drawing.Point(20, 60);
            this.optionPanel.Name = "optionPanel";
            this.optionPanel.Size = new System.Drawing.Size(174, 466);
            this.optionPanel.TabIndex = 4;
            this.optionPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.optionPanel.VerticalScrollbarBarColor = true;
            this.optionPanel.VerticalScrollbarHighlightOnWheel = false;
            this.optionPanel.VerticalScrollbarSize = 10;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.Black;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(15, 173);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(156, 41);
            this.btnLogout.TabIndex = 5;
            this.btnLogout.Text = "    Logout";
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // metroTile
            // 
            this.metroTile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(225)))));
            this.metroTile.Location = new System.Drawing.Point(3, 3);
            this.metroTile.Name = "metroTile";
            this.metroTile.Size = new System.Drawing.Size(10, 51);
            this.metroTile.TabIndex = 2;
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.Color.White;
            this.btnUsers.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUsers.FlatAppearance.BorderSize = 0;
            this.btnUsers.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnUsers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsers.ForeColor = System.Drawing.Color.Black;
            this.btnUsers.Image = ((System.Drawing.Image)(resources.GetObject("btnUsers.Image")));
            this.btnUsers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUsers.Location = new System.Drawing.Point(15, 3);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(156, 51);
            this.btnUsers.TabIndex = 3;
            this.btnUsers.Text = "   All Requirments";
            this.btnUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // UserDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 546);
            this.Controls.Add(this.parentPanel);
            this.Controls.Add(this.optionPanel);
            this.Name = "UserDashboard";
            this.Text = "UserDashboard";
            this.Load += new System.EventHandler(this.UserDashboard_Load);
            this.optionPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel parentPanel;
        private MetroFramework.Controls.MetroPanel optionPanel;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel metroTile;
        private System.Windows.Forms.Button btnUsers;
    }
}