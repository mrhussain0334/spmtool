﻿using System;
using System.Linq;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms.User
{
    public partial class ManageUser : MetroForm
    {
        private bool _isEdit;
        private int _userId;
        private Models.User userObj;
        public ManageUser(bool isEdit, int userId = 0)
        {
            _isEdit = isEdit;
            _userId = userId;
            InitializeComponent();
        }

        private void CreateUser_Load(object sender, EventArgs e)
        {

            rbMale.Checked = true; 
            if (_isEdit)
            {
                Controls.Remove(txtPassword);
                Controls.Remove(txtUsername);
                Controls.Remove(txtVerifyPassword);
                Text = @"Create User";
                if (_userId > 0)
                {
                    var repo = new UserRepository();
                    userObj = repo.GetUserById(_userId);
                    SetValues();
                    btnCreate.Text = @"Update User";
                }
                else
                {
                    MessagBoxService.Show(this, "Error Occured", "Error");
                    Close();
                }
            }
            else
            {
                Text = @"Update User";
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                if (VerifyAdd())
                {
                    MessagBoxService.Show(this, "User Added", "");
                    Reset();
                }
            }
            else
            {
                if (VerifyUpdate())
                {
                    MessagBoxService.Show(this, "User Updated", "");
                    Close();
                }
            }
        }


        private void Reset()
        {
            var list = Controls;
            foreach (Control control in list)
            {
                if (control.GetType() == typeof(MetroTextBox))
                {
                    control.Text = "";
                }
            }
        }
        private bool VerifyAdd()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            var repo = new ProjectManagerRepository();
            if (!repo.VerifyIfUsernameAvailable(txtUsername.Text))
            {
                MetroMessageBox.Show(this, "Username Already Exists", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Asterisk);
                return false;
            }
            if (txtPassword.Text != txtVerifyPassword.Text)
            {
                return false;
            }
            var manager = new Models.User()
            {
                Address = txtAddress.Text,
                CreatedAt = DateTime.Now,
                CreatedByProjectManagerId = ConfigService.Instance.Id,
                Email = txtEmail.Text,
                FirstName = txtFName.Text,
                Gender = rbMale.Checked ? "Male" : "Female",
                LastName = txtLName.Text,
                Username = txtUsername.Text,
                Password = PasswordService.EncryptPassword(txtPassword.Text),
                Designation = cbxDesignation.SelectedItem.ToString(),
                UpdateAt = DateTime.Now
            };

            repo.Add(manager);
            return true;
        }
        private bool VerifyUpdate()
        {
            var list = Controls;
            if ((from Control control in list where control.GetType() == typeof(MetroTextBox) select (MetroTextBox)control).Any(txtbox => txtbox.Text.Length < 1))
            {
                MessagBoxService.Show(this, "Fill All the Boxes", "Error");
                return false;
            }
            var repo = new UserRepository();
            if (!repo.VerifyIfUsernameAvailable(txtUsername.Text))
            {
                MetroMessageBox.Show(this, "Username Already Exists", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Asterisk);
                return false;
            }
            var userObj = new Models.User()
            {
                Address = txtAddress.Text,
                CreatedAt = DateTime.Now,
                CreatedByProjectManagerId = ConfigService.Instance.Id,
                Email = txtEmail.Text,
                FirstName = txtFName.Text,
                Gender = rbMale.Checked ? "Male" : "Female",
                LastName = txtLName.Text,
                Username = txtUsername.Text,
                Password = PasswordService.EncryptPassword(txtPassword.Text),
                UpdateAt = DateTime.Now,
                Designation = cbxDesignation.SelectedItem.ToString()
            };
            repo.Update(userObj, _userId);
            return true;
        }

        private void SetValues()
        {
            txtFName.Text = userObj.FirstName;
            txtLName.Text = userObj.LastName;
            txtAddress.Text = userObj.Address;
            cbxDesignation.SelectedIndex = cbxDesignation.FindStringExact(userObj.Designation);
            txtEmail.Text = userObj.Email;
            if (userObj.Gender == "Male")
            {
                rbMale.Checked = true;
                rbFemale.Checked = false;
            }
            else
            {
                rbMale.Checked = false;
                rbFemale.Checked = true;
            }
        }

    }
}
