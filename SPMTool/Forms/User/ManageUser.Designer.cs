﻿namespace SPMTool.Forms.User
{
    partial class ManageUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.rbFemale = new MetroFramework.Controls.MetroRadioButton();
            this.rbMale = new MetroFramework.Controls.MetroRadioButton();
            this.txtVerifyPassword = new MetroFramework.Controls.MetroTextBox();
            this.txtPassword = new MetroFramework.Controls.MetroTextBox();
            this.txtUsername = new MetroFramework.Controls.MetroTextBox();
            this.txtAddress = new MetroFramework.Controls.MetroTextBox();
            this.txtLName = new MetroFramework.Controls.MetroTextBox();
            this.txtFName = new MetroFramework.Controls.MetroTextBox();
            this.btnReset = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnCreate = new MetroFramework.Controls.MetroButton();
            this.cbxDesignation = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // txtEmail
            // 
            // 
            // 
            // 
            this.txtEmail.CustomButton.Image = null;
            this.txtEmail.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.txtEmail.CustomButton.Name = "";
            this.txtEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtEmail.CustomButton.TabIndex = 1;
            this.txtEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtEmail.CustomButton.UseSelectable = true;
            this.txtEmail.CustomButton.Visible = false;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(289, 63);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.PromptText = "Enter Email";
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.SelectionLength = 0;
            this.txtEmail.SelectionStart = 0;
            this.txtEmail.ShortcutsEnabled = true;
            this.txtEmail.Size = new System.Drawing.Size(260, 23);
            this.txtEmail.TabIndex = 20;
            this.txtEmail.UseSelectable = true;
            this.txtEmail.WaterMark = "Enter Email";
            this.txtEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(289, 185);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 19);
            this.metroLabel1.TabIndex = 27;
            this.metroLabel1.Text = "Select Gender";
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(344, 207);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(61, 15);
            this.rbFemale.TabIndex = 25;
            this.rbFemale.Text = "Female";
            this.rbFemale.UseSelectable = true;
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Location = new System.Drawing.Point(289, 207);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(49, 15);
            this.rbMale.TabIndex = 24;
            this.rbMale.Text = "Male";
            this.rbMale.UseSelectable = true;
            // 
            // txtVerifyPassword
            // 
            // 
            // 
            // 
            this.txtVerifyPassword.CustomButton.Image = null;
            this.txtVerifyPassword.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.txtVerifyPassword.CustomButton.Name = "";
            this.txtVerifyPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtVerifyPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtVerifyPassword.CustomButton.TabIndex = 1;
            this.txtVerifyPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtVerifyPassword.CustomButton.UseSelectable = true;
            this.txtVerifyPassword.CustomButton.Visible = false;
            this.txtVerifyPassword.Lines = new string[0];
            this.txtVerifyPassword.Location = new System.Drawing.Point(289, 150);
            this.txtVerifyPassword.MaxLength = 32767;
            this.txtVerifyPassword.Name = "txtVerifyPassword";
            this.txtVerifyPassword.PasswordChar = '*';
            this.txtVerifyPassword.PromptText = "Verify Password";
            this.txtVerifyPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVerifyPassword.SelectedText = "";
            this.txtVerifyPassword.SelectionLength = 0;
            this.txtVerifyPassword.SelectionStart = 0;
            this.txtVerifyPassword.ShortcutsEnabled = true;
            this.txtVerifyPassword.Size = new System.Drawing.Size(260, 23);
            this.txtVerifyPassword.TabIndex = 23;
            this.txtVerifyPassword.UseSelectable = true;
            this.txtVerifyPassword.WaterMark = "Verify Password";
            this.txtVerifyPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtVerifyPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPassword
            // 
            // 
            // 
            // 
            this.txtPassword.CustomButton.Image = null;
            this.txtPassword.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.txtPassword.CustomButton.Name = "";
            this.txtPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPassword.CustomButton.TabIndex = 1;
            this.txtPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPassword.CustomButton.UseSelectable = true;
            this.txtPassword.CustomButton.Visible = false;
            this.txtPassword.Lines = new string[0];
            this.txtPassword.Location = new System.Drawing.Point(289, 121);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.PromptText = "Enter Password (alphanumeric, special)";
            this.txtPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPassword.SelectedText = "";
            this.txtPassword.SelectionLength = 0;
            this.txtPassword.SelectionStart = 0;
            this.txtPassword.ShortcutsEnabled = true;
            this.txtPassword.Size = new System.Drawing.Size(260, 23);
            this.txtPassword.TabIndex = 22;
            this.txtPassword.UseSelectable = true;
            this.txtPassword.WaterMark = "Enter Password (alphanumeric, special)";
            this.txtPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtUsername
            // 
            // 
            // 
            // 
            this.txtUsername.CustomButton.Image = null;
            this.txtUsername.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.txtUsername.CustomButton.Name = "";
            this.txtUsername.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUsername.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUsername.CustomButton.TabIndex = 1;
            this.txtUsername.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUsername.CustomButton.UseSelectable = true;
            this.txtUsername.CustomButton.Visible = false;
            this.txtUsername.Lines = new string[0];
            this.txtUsername.Location = new System.Drawing.Point(289, 92);
            this.txtUsername.MaxLength = 32767;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.PasswordChar = '\0';
            this.txtUsername.PromptText = "Enter Username";
            this.txtUsername.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUsername.SelectedText = "";
            this.txtUsername.SelectionLength = 0;
            this.txtUsername.SelectionStart = 0;
            this.txtUsername.ShortcutsEnabled = true;
            this.txtUsername.Size = new System.Drawing.Size(260, 23);
            this.txtUsername.TabIndex = 21;
            this.txtUsername.UseSelectable = true;
            this.txtUsername.WaterMark = "Enter Username";
            this.txtUsername.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUsername.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtAddress
            // 
            // 
            // 
            // 
            this.txtAddress.CustomButton.Image = null;
            this.txtAddress.CustomButton.Location = new System.Drawing.Point(204, 2);
            this.txtAddress.CustomButton.Name = "";
            this.txtAddress.CustomButton.Size = new System.Drawing.Size(53, 53);
            this.txtAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAddress.CustomButton.TabIndex = 1;
            this.txtAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAddress.CustomButton.UseSelectable = true;
            this.txtAddress.CustomButton.Visible = false;
            this.txtAddress.Lines = new string[0];
            this.txtAddress.Location = new System.Drawing.Point(23, 121);
            this.txtAddress.MaxLength = 32767;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.PasswordChar = '\0';
            this.txtAddress.PromptText = "Enter Address";
            this.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAddress.SelectedText = "";
            this.txtAddress.SelectionLength = 0;
            this.txtAddress.SelectionStart = 0;
            this.txtAddress.ShortcutsEnabled = true;
            this.txtAddress.Size = new System.Drawing.Size(260, 58);
            this.txtAddress.TabIndex = 17;
            this.txtAddress.UseSelectable = true;
            this.txtAddress.WaterMark = "Enter Address";
            this.txtAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLName
            // 
            // 
            // 
            // 
            this.txtLName.CustomButton.Image = null;
            this.txtLName.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.txtLName.CustomButton.Name = "";
            this.txtLName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLName.CustomButton.TabIndex = 1;
            this.txtLName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLName.CustomButton.UseSelectable = true;
            this.txtLName.CustomButton.Visible = false;
            this.txtLName.Lines = new string[0];
            this.txtLName.Location = new System.Drawing.Point(23, 92);
            this.txtLName.MaxLength = 32767;
            this.txtLName.Name = "txtLName";
            this.txtLName.PasswordChar = '\0';
            this.txtLName.PromptText = "Enter Last Name";
            this.txtLName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLName.SelectedText = "";
            this.txtLName.SelectionLength = 0;
            this.txtLName.SelectionStart = 0;
            this.txtLName.ShortcutsEnabled = true;
            this.txtLName.Size = new System.Drawing.Size(260, 23);
            this.txtLName.TabIndex = 16;
            this.txtLName.UseSelectable = true;
            this.txtLName.WaterMark = "Enter Last Name";
            this.txtLName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtFName
            // 
            // 
            // 
            // 
            this.txtFName.CustomButton.Image = null;
            this.txtFName.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.txtFName.CustomButton.Name = "";
            this.txtFName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtFName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFName.CustomButton.TabIndex = 1;
            this.txtFName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFName.CustomButton.UseSelectable = true;
            this.txtFName.CustomButton.Visible = false;
            this.txtFName.Lines = new string[0];
            this.txtFName.Location = new System.Drawing.Point(23, 63);
            this.txtFName.MaxLength = 32767;
            this.txtFName.Name = "txtFName";
            this.txtFName.PasswordChar = '\0';
            this.txtFName.PromptText = "Enter First Name";
            this.txtFName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFName.SelectedText = "";
            this.txtFName.SelectionLength = 0;
            this.txtFName.SelectionStart = 0;
            this.txtFName.ShortcutsEnabled = true;
            this.txtFName.Size = new System.Drawing.Size(260, 23);
            this.txtFName.TabIndex = 15;
            this.txtFName.UseSelectable = true;
            this.txtFName.WaterMark = "Enter First Name";
            this.txtFName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtFName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(127, 334);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 28;
            this.btnReset.Text = "Reset";
            this.btnReset.UseSelectable = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(208, 334);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 29;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(23, 334);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(98, 23);
            this.btnCreate.TabIndex = 26;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseSelectable = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // cbxDesignation
            // 
            this.cbxDesignation.FormattingEnabled = true;
            this.cbxDesignation.ItemHeight = 23;
            this.cbxDesignation.Items.AddRange(new object[] {
            "Programmer",
            "Tester",
            "Designer"});
            this.cbxDesignation.Location = new System.Drawing.Point(23, 207);
            this.cbxDesignation.Name = "cbxDesignation";
            this.cbxDesignation.Size = new System.Drawing.Size(260, 29);
            this.cbxDesignation.TabIndex = 30;
            this.cbxDesignation.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 185);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(115, 19);
            this.metroLabel2.TabIndex = 31;
            this.metroLabel2.Text = "Select Designation";
            // 
            // ManageUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 370);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.cbxDesignation);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.rbFemale);
            this.Controls.Add(this.rbMale);
            this.Controls.Add(this.txtVerifyPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.txtLName);
            this.Controls.Add(this.txtFName);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Name = "ManageUser";
            this.Text = "Create User";
            this.Load += new System.EventHandler(this.CreateUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox txtEmail;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroRadioButton rbFemale;
        private MetroFramework.Controls.MetroRadioButton rbMale;
        private MetroFramework.Controls.MetroTextBox txtVerifyPassword;
        private MetroFramework.Controls.MetroTextBox txtPassword;
        private MetroFramework.Controls.MetroTextBox txtUsername;
        private MetroFramework.Controls.MetroTextBox txtAddress;
        private MetroFramework.Controls.MetroTextBox txtLName;
        private MetroFramework.Controls.MetroTextBox txtFName;
        private MetroFramework.Controls.MetroButton btnReset;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroButton btnCreate;
        private MetroFramework.Controls.MetroComboBox cbxDesignation;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}