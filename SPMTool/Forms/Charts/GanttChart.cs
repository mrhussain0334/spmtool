﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Braincase.GanttChart;
using MetroFramework.Forms;
using SPMTool.Models;
using ProjectManager = Braincase.GanttChart.ProjectManager;

namespace SPMTool.Forms.Charts
{
    public partial class GanttChart : MetroForm
    {
        private readonly List<Requirement> _requirements;

        public GanttChart(List<Requirement> requirements)
        {
            _requirements = requirements;
            InitializeComponent();
        }

        private void GanttChart_Load(object sender, EventArgs e)
        {
            var list = new List<Test>();
            var m = new ProjectManager();
            WindowState = FormWindowState.Maximized;
            foreach (var requirement in _requirements)
            {
                var t = new Task {Name = requirement.Code};
                
                list.Add(new Test()
                {
                    Requirement = requirement,
                    Task = t,
                    Start = 0
                });
                m.Add(t);
                if (requirement.IsCompleted)
                {
                    m.SetComplete(t, 1);
                }
                m.SetDuration(t, int.Parse(requirement.Deadline));
            }

            foreach (var test in list)
            {
                try
                {
                    if (test.Requirement.RequirementDependencies.Any())
                    {
                        foreach (var requirementRequirement in test.Requirement.RequirementDependencies)
                        {
                            var d =
                                list.FirstOrDefault(p => p.Requirement.Code == requirementRequirement.Requirement.Code);
                            if (d != null)
                            {
                                //if (test.Total > d.Start)
                                //{
                                //    m.SetStart(d.Task, test.Total);
                                //    d.Start = test.Total;
                                //}
                                m.Relate(test.Task,d.Task);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            chart1.Init(m);
        }

    }

    public class Test
    {
        public Task Task { get; set; }
        public Requirement Requirement { get; set; }
        public int Start { get; set; }
        public int Total => Start + Task.Duration;
    }
}