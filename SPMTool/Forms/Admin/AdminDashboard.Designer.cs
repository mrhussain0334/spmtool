﻿namespace SPMTool.Forms
{
    partial class AdminDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminDashboard));
            this.optionPanel = new MetroFramework.Controls.MetroPanel();
            this.btnAllProjects = new System.Windows.Forms.Button();
            this.metroTile = new System.Windows.Forms.Panel();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnAllProjectManager = new System.Windows.Forms.Button();
            this.parentPanel = new MetroFramework.Controls.MetroPanel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.optionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // optionPanel
            // 
            this.optionPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.optionPanel.Controls.Add(this.btnLogout);
            this.optionPanel.Controls.Add(this.btnAllProjects);
            this.optionPanel.Controls.Add(this.metroTile);
            this.optionPanel.Controls.Add(this.btnUsers);
            this.optionPanel.Controls.Add(this.btnAllProjectManager);
            this.optionPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.optionPanel.HorizontalScrollbarBarColor = true;
            this.optionPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.optionPanel.HorizontalScrollbarSize = 10;
            this.optionPanel.Location = new System.Drawing.Point(20, 60);
            this.optionPanel.Name = "optionPanel";
            this.optionPanel.Size = new System.Drawing.Size(174, 451);
            this.optionPanel.TabIndex = 0;
            this.optionPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.optionPanel.VerticalScrollbarBarColor = true;
            this.optionPanel.VerticalScrollbarHighlightOnWheel = false;
            this.optionPanel.VerticalScrollbarSize = 10;
            // 
            // btnAllProjects
            // 
            this.btnAllProjects.BackColor = System.Drawing.Color.White;
            this.btnAllProjects.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAllProjects.FlatAppearance.BorderSize = 0;
            this.btnAllProjects.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnAllProjects.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnAllProjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllProjects.ForeColor = System.Drawing.Color.Black;
            this.btnAllProjects.Image = ((System.Drawing.Image)(resources.GetObject("btnAllProjects.Image")));
            this.btnAllProjects.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAllProjects.Location = new System.Drawing.Point(18, 107);
            this.btnAllProjects.Name = "btnAllProjects";
            this.btnAllProjects.Size = new System.Drawing.Size(156, 41);
            this.btnAllProjects.TabIndex = 4;
            this.btnAllProjects.Text = "    All Projects";
            this.btnAllProjects.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAllProjects.UseVisualStyleBackColor = false;
            this.btnAllProjects.Click += new System.EventHandler(this.btnAllProjects_Click);
            // 
            // metroTile
            // 
            this.metroTile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(225)))));
            this.metroTile.Location = new System.Drawing.Point(3, 3);
            this.metroTile.Name = "metroTile";
            this.metroTile.Size = new System.Drawing.Size(10, 51);
            this.metroTile.TabIndex = 2;
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.Color.White;
            this.btnUsers.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUsers.FlatAppearance.BorderSize = 0;
            this.btnUsers.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnUsers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsers.ForeColor = System.Drawing.Color.Black;
            this.btnUsers.Image = ((System.Drawing.Image)(resources.GetObject("btnUsers.Image")));
            this.btnUsers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUsers.Location = new System.Drawing.Point(15, 60);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(156, 41);
            this.btnUsers.TabIndex = 3;
            this.btnUsers.Text = "    All Users";
            this.btnUsers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnProjects_Click);
            // 
            // btnAllProjectManager
            // 
            this.btnAllProjectManager.BackColor = System.Drawing.Color.White;
            this.btnAllProjectManager.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAllProjectManager.FlatAppearance.BorderSize = 0;
            this.btnAllProjectManager.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnAllProjectManager.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnAllProjectManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllProjectManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllProjectManager.ForeColor = System.Drawing.Color.Black;
            this.btnAllProjectManager.Image = ((System.Drawing.Image)(resources.GetObject("btnAllProjectManager.Image")));
            this.btnAllProjectManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAllProjectManager.Location = new System.Drawing.Point(15, 3);
            this.btnAllProjectManager.Name = "btnAllProjectManager";
            this.btnAllProjectManager.Size = new System.Drawing.Size(156, 51);
            this.btnAllProjectManager.TabIndex = 2;
            this.btnAllProjectManager.Text = "   All  Managers";
            this.btnAllProjectManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAllProjectManager.UseVisualStyleBackColor = false;
            this.btnAllProjectManager.Click += new System.EventHandler(this.btnAllUsers_Click);
            // 
            // parentPanel
            // 
            this.parentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parentPanel.HorizontalScrollbarBarColor = true;
            this.parentPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.parentPanel.HorizontalScrollbarSize = 10;
            this.parentPanel.Location = new System.Drawing.Point(194, 60);
            this.parentPanel.Name = "parentPanel";
            this.parentPanel.Size = new System.Drawing.Size(697, 451);
            this.parentPanel.TabIndex = 1;
            this.parentPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.parentPanel.VerticalScrollbarBarColor = true;
            this.parentPanel.VerticalScrollbarHighlightOnWheel = false;
            this.parentPanel.VerticalScrollbarSize = 10;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.Black;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(18, 154);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(156, 41);
            this.btnLogout.TabIndex = 5;
            this.btnLogout.Text = "    Logout";
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // AdminDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 531);
            this.Controls.Add(this.parentPanel);
            this.Controls.Add(this.optionPanel);
            this.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Name = "AdminDashboard";
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "AdminDashboard";
            this.Load += new System.EventHandler(this.AdminDashboard_Load);
            this.optionPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel optionPanel;
        private MetroFramework.Controls.MetroPanel parentPanel;
        private System.Windows.Forms.Button btnAllProjectManager;
        private System.Windows.Forms.Button btnUsers;
        private System.Windows.Forms.Panel metroTile;
        private System.Windows.Forms.Button btnAllProjects;
        private System.Windows.Forms.Button btnLogout;
    }
}