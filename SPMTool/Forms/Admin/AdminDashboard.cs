﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using SPMTool.Services;
using SPMTool.UserControls;

namespace SPMTool.Forms
{
    public partial class AdminDashboard : MetroForm
    {
        public AdminDashboard()
        {
            InitializeComponent();
        }

        private void btnAllUsers_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnAllProjectManager);
            Clear();
            parentPanel.Controls.Add(new AllAdminProjectManager());
        }

        
        private void btnProjects_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnUsers);
            Clear();
            parentPanel.Controls.Add(new AllUsers(true));
        }

        private void AdminDashboard_Load(object sender, EventArgs e)
        {
            SetTileLocation(btnAllProjectManager);
            Clear();
            var ctrl = new AllAdminProjectManager();
            parentPanel.Controls.Add(ctrl);
        }
        private void btnAllProjects_Click(object sender, EventArgs e)
        {
            SetTileLocation(btnAllProjects);
            Clear();
            var ctrl = new AllProjects(true);
            parentPanel.Controls.Add(ctrl);

        }
        #region OtherMethods

        private void SetTileLocation(Control btn)
        {
            metroTile.Top = btn.Top;
            metroTile.Height = btn.Height;
        }
        void Clear()
        {
            parentPanel.Controls.Clear();
        }


        #endregion

        private void btnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Abort;
                Close();
            }
            catch (Exception exception)
            {
                MessagBoxService.Show(this,exception);
            }
        }
    }
}
