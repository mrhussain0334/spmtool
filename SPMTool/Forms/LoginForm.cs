﻿using System;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using SPMTool.Forms.User;
using SPMTool.Repository;
using SPMTool.Services;

namespace SPMTool.Forms
{
    public partial class LoginForm : MetroForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private const bool isIntro = false;
        private void LoginForm_Load(object sender, EventArgs e)
        {

            if (isIntro)
            {
                var pg = new IntroStartupForm();
                pg.ShowDialog();
            }
            cbxLoginType.SelectedIndex = 0;
            WindowState = FormWindowState.Normal;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text.Length < 1 || txtPassword.Text.Length < 1)
            {
                MetroMessageBox.Show(this, "Please Fill Username/Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            if (cbxLoginType.SelectedIndex < 0)
            {
                MetroMessageBox.Show(this, "Please Select Login Type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            DialogResult result = DialogResult.None;
            switch (cbxLoginType.SelectedIndex)
            {
                case 0:
                    {
                        var user = new AdminRepository().Login(txtUsername.Text.Trim(), txtPassword.Text.Trim());
                        if (user == null)
                        {
                            MetroMessageBox.Show(this, "Wrong Username/Password", "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
                            return;
                        }
                        Hide();
                        ConfigService.Instance.SetValues(user);
                        var adminform = new AdminDashboard();
                        result = adminform.ShowDialog();
                        if (result == DialogResult.Abort)
                        {
                            Show();
                            Reset();
                        }
                        else
                        {
                            Close();
                        }
                        break;
                    }
                case 1:
                    {
                        var user = new ProjectManagerRepository().Login(txtUsername.Text.Trim(), txtPassword.Text.Trim());

                        if (user == null)
                        {
                            MetroMessageBox.Show(this, "Wrong Username/Password", "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
                            return;
                        }
                        Hide();
                        ConfigService.Instance.SetValues(user);
                        var adminform = new ProjectManagerDashboard();
                        result = adminform.ShowDialog();
                        if (result == DialogResult.Abort)
                        {
                            Show();
                            Reset();
                        }
                        else
                        {
                            Close();
                        }
                        break;
                    }
                case 2:
                    {
                        var user = new UserRepository().Login(txtUsername.Text.Trim(), txtPassword.Text.Trim());

                        if (user == null)
                        {
                            MetroMessageBox.Show(this, "Wrong Username/Password", "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
                            return;
                        }
                        Hide();
                        ConfigService.Instance.SetValues(user);
                        var adminform = new UserDashboard();
                        result = adminform.ShowDialog();
                        if (result == DialogResult.Abort)
                        {
                            Show();
                            Reset();
                        }
                        else
                        {
                            Close();
                        }
                        break;
                    }
                default:
                    break;
            }
        }

        private void Reset()
        {
            txtPassword.Text = txtUsername.Text = "";
            cbxLoginType.SelectedIndex = 0;
            txtUsername.Focus();
        }
        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnLogin_Click(sender, e);
            }
        }
    }
}
