﻿using System.Data.Entity;
using SPMTool.Models;

namespace SPMTool.Context
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("SPMTool")
        {
            
        }
        public DbSet<User> Users { get; set; }
        public DbSet<ParentRequriement> ParentRequriements { get; set; }
        public DbSet<ProjectType> ProjectTypes { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Requirement> Requirements { get; set; }
        public DbSet<RequirementType> RequirementTypes { get; set; }
        public DbSet<UserRequirement> UserRequirements { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<ProjectDomain> ProjectDomains { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<ProjectClient> ProjectClients { get; set; }
        public DbSet<ProjectManager> ProjectManagers { get; set; }
        public DbSet<ProjectCharter> ProjectCharters { get; set; }
        public DbSet<Wbs> Wbses { get; set; }
        public DbSet<RequirementDependency> RequirementDependencies { get; set; }
    }
}
