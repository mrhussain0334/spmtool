﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class ProjectTypeRepository : BaseRepository
    {
        public IEnumerable<ProjectType> GetAllTypes()
        {
            try
            {
                return Db.ProjectTypes.ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
