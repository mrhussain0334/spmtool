﻿using System;
using SPMTool.Context;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class BaseRepository : IDisposable
    {
        protected internal MyDbContext Db; 
        public BaseRepository()
        {
            Db = new MyDbContext();
        }
        ~BaseRepository()
        {
            CleanUp();
        }

        public void Add<T>(T entity) where T : Base
        {
            try
            {
                Db.Set<T>().Add(entity);
                Db.SaveChanges();
            }
            catch (Exception e)
            {
                throw;
            };
        }
        
        public void Dispose()
        {
            CleanUp();
            GC.SuppressFinalize(this);
        }

        private void CleanUp()
        {
            Db?.Dispose();
        }
    }
}
