﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class ProjectDomainRepository : BaseRepository
    {
        public IEnumerable<ProjectDomain> GetAllDomains()
        {
            try
            {
                return Db.ProjectDomains.ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
