﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class RequirementTypeRepository : BaseRepository
    {
        public IEnumerable<RequirementType> GetaRequirementTypes()
        {
            try
            {
                return Db.RequirementTypes.ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
