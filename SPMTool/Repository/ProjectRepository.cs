﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class ProjectRepository : BaseRepository
    {
        public IEnumerable<Project> GetAllProjects()
        {
            try
            {
                return Db.Projects.ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public IEnumerable<Project> GetAllProjectsByCreater(int id)
        {
            try
            {
                return Db.Projects.Where(p => p.ProjectManagerId == id).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public Project GetProjectById(int id)
        {
            try
            {
                return Db.Projects.FirstOrDefault(p => p.Id == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool DeleteProjectById(int id)
        {
            try
            {
                var d =  Db.Projects.FirstOrDefault(p => p.Id == id);
                Db.Entry(d).State = EntityState.Deleted;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool Update(Project project, int projectId)
        {
            try
            {
                var old = GetProjectById(projectId);
                old.Name = project.Name;
                old.ProjectTypeId = project.ProjectTypeId;
                old.ProjectDomainId = project.ProjectDomainId;
                old.Description = project.Description;
                old.UpdateAt = DateTime.Now;
                old.ProjectDomainId = project.ProjectDomainId;
                old.ProjectTypeId = project.ProjectTypeId;
                Db.Entry(old).State = EntityState.Modified;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
