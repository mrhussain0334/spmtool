﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class RequirementRepository : BaseRepository
    {
        public List<Requirement> GetRequirementsByProjectId(int projectId)
        {
            try
            {
                return Db.Requirements.Where(p => p.ProjectId == projectId).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool CheckIfKeyExist(string key,int projectId)
        {
            try
            {
                return Db.Requirements.Any(p => p.ProjectId == projectId && p.Code == key);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                var req =  Db.Requirements.Find(id);
                Db.Entry(req).State = EntityState.Deleted;
                Db.SaveChanges();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public Requirement GetRequirementsByCodeByProjectId(string code, int projectId)
        {
            try
            {
                return Db.Requirements.FirstOrDefault(p => p.Code == code && p.ProjectId == projectId);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public Requirement GetRequirementById(int parse)
        {
            try
            {
                return Db.Requirements.Find(parse);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public List<Requirement> GetRequirementByUserId(int parse)
        {
            try
            {
                return Db.Requirements.Where(p => p.AssignedToUserId.Value == parse).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool DeleteRequirement(int id)
        {
            try
            {
                var d = GetRequirementById(id);
                if (d != null)
                {
                    var deps = d.Requirements;
                    foreach (var requirementDependency in deps)
                    {
                        Delete(requirementDependency.Id);
                    }
                    Delete(d.Id);
                }
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void Update(Requirement requirement)
        {
            try
            {
                Db.Entry(requirement).State = EntityState.Modified;
                Db.SaveChanges();
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
