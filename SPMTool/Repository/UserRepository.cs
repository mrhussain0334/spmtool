﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SPMTool.Models;
using SPMTool.Services;

namespace SPMTool.Repository
{
    public class UserRepository : BaseRepository
    {

        public User Login(string username, string password)
        {
            try
            {
                var pass = PasswordService.EncryptPassword(password);
                var user = Db.Users.FirstOrDefault(p => p.Username == username && p.Password == pass);
                return user;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                return Db.Users.ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IEnumerable<User> GetAllUsersByProjectManagerId()
        {
            try
            {
                return Db.Users.Where(p => p.CreatedByProjectManagerId == ConfigService.Instance.Id).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public IEnumerable<User> GetAllUsersByProjectManagerId(int id)
        {
            try
            {
                return Db.Users.Where(p => p.CreatedByProjectManagerId == id).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public bool VerifyIfUsernameAvailable(string username)
        {
            try
            {
                return Db.Users.FirstOrDefault(p => p.Username == username) == null;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public User GetUserById(int userId)
        {
            try
            {
                return Db.Users.FirstOrDefault(p => p.Id == userId);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool Update(User userObj, int userId)
        {
            try
            {
                var oldManager = GetUserById(userId);
                if (oldManager == null) return false;
                oldManager.Address = userObj.Address;
                oldManager.Email = userObj.Email;
                oldManager.FirstName = userObj.FirstName;
                oldManager.LastName = userObj.LastName;
                oldManager.Designation = userObj.Designation;
                oldManager.UpdateAt = DateTime.Now;
                Db.Entry(oldManager).State = EntityState.Modified;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var obj = GetUserById(id);
                if (obj == null) return false;
                Db.Entry(obj).State = EntityState.Deleted;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool UpdateCredentials(User user, int userId)
        {
            try
            {
                var oldManager = GetUserById(userId);
                if (oldManager == null) return false;
                oldManager.Username = user.Username;
                oldManager.Password = user.Password;
                oldManager.UpdateAt = DateTime.Now;
                Db.Entry(oldManager).State = EntityState.Modified;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
