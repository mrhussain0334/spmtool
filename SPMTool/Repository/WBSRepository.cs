﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class WbsRepository : BaseRepository
    {
        public IEnumerable<Wbs> GetWbsesByProjectId(int projectId)
        {
            try
            {
                return Db.Wbses.Where(p => p.ProjectId == projectId).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool CheckIfExist(string code,int projectId)
        {
            try
            {
                return Db.Wbses.Any(p => p.ProjectId == projectId && p.Code == code);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var wbs = Db.Wbses.Find(id);
                Db.Entry(wbs).State = EntityState.Deleted;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
