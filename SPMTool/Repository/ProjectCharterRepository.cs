﻿using System;
using System.Linq;
using SPMTool.Models;

namespace SPMTool.Repository
{
    public class ProjectCharterRepository : BaseRepository
    {
        public ProjectCharter GetById(int id)
        {
            try
            {
                return Db.ProjectCharters.FirstOrDefault(p => p.Id == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool Update(ProjectCharter newObj, int id)
        {
            try
            {
                var obj = GetById(id);
                obj.Assumptions = newObj.Assumptions;
                obj.Author = newObj.Author;
                obj.BudgetRequirements = newObj.BudgetRequirements;
                obj.Description = newObj.Description;
                obj.Scope = newObj.Scope;
                obj.EndDate = newObj.EndDate;
                obj.Sponser = newObj.Sponser;
                obj.StakeHolders = newObj.StakeHolders;
                obj.StartDate = newObj.StartDate;
                obj.ProjectName = obj.ProjectName;
                obj.Risks = newObj.Risks;
                obj.Requester = newObj.Requester;
                obj.Purpose = newObj.Purpose;
                obj.Goals = newObj.Goals;
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public ProjectCharter GetProjectCharterByProjectId(int projectId)
        {
            try
            {
                return Db.ProjectCharters.FirstOrDefault(p => p.ProjectId == projectId);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
