﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SPMTool.Models;
using SPMTool.Services;

namespace SPMTool.Repository
{
    public class ProjectManagerRepository : BaseRepository
    {
        public ProjectManager Login(string username, string password)
        {
            try
            {
                var checkpassword = PasswordService.EncryptPassword(password);
                var user = Db.ProjectManagers.FirstOrDefault(p => p.Username == username && p.Password == checkpassword);
                return user;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool UpdateCredentials(ProjectManager manager, int id)
        {
            try
            {
                var oldManager = GetById(id);
                if (oldManager == null) return false;
                oldManager.Username = manager.Username;
                oldManager.Password = manager.Password;
                oldManager.UpdateAt = DateTime.Now;
                Db.Entry(oldManager).State = EntityState.Modified;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public bool Update(ProjectManager manager, int id)
        {
            try
            {
                var oldManager = GetById(id);
                if (oldManager == null) return false;
                oldManager.Address = manager.Address;
                oldManager.ContactNumber = manager.ContactNumber;
                oldManager.Email = manager.Email;
                oldManager.FirstName = manager.FirstName;
                oldManager.LastName = manager.LastName;
                oldManager.Qualifications = manager.Qualifications;
                oldManager.UpdateAt = DateTime.Now;
                Db.Entry(oldManager).State = EntityState.Modified;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public ProjectManager GetById(int id)
        {
            try
            {
                return Db.ProjectManagers.FirstOrDefault(p => p.Id == id);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IEnumerable<ProjectManager> GetAllManagers()
        {
            try
            {
                var user = Db.ProjectManagers.Where(p => p.CreatedByAdminId == ConfigService.Instance.Id).ToList();
                return user;
            }
            catch (Exception e)
            {
                throw;
            }
        }


        public bool VerifyIfUsernameAvailable(string username)
        {
            try
            {
                return Db.ProjectManagers.FirstOrDefault(p => p.Username == username) == null;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var obj = GetById(id);
                if (obj == null) return false;
                Db.Entry(obj).State = EntityState.Deleted;
                Db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
