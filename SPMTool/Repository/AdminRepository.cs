﻿using System;
using System.Linq;
using SPMTool.Models;
using SPMTool.Services;

namespace SPMTool.Repository
{
    public class AdminRepository : BaseRepository
    {
        public Admin Login(string username, string password)
        {
            try
            {
                var checkpassword = PasswordService.EncryptPassword(password);
                return Db.Admins.FirstOrDefault(p => p.Username == username && p.Password == checkpassword);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
